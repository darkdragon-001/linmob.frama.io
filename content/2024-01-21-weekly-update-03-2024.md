+++
title = "Weekly GNU-like Mobile Linux Update (03/2024): OG PinePhone WiFi Progress"
date = "2024-01-21T17:30:01Z"
draft = false 
[taxonomies]
tags = ["Sailfish OS", "Ubuntu Touch", "postmarketOS", "Mobian", "RTW8723CS", "PinePhone", "KDE Itinerary", "Librem 5",]
categories = ["weekly update"]
authors = ["Peter",]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Another week, another update: The RTW8723CS driver progresses nicely, a Year in Review post by Mobian, releases of libdng and libmegapixels, Sailfish OS Community News, another Ubuntu Touch Q&A and more. 

<!-- more -->
_Commentary in italics._

### Software

#### Gnome Ecosystem
- This Week in GNOME: [#131 STF Happenings](https://thisweek.gnome.org/posts/2024/01/twig-131/)

#### Phosh
- Guido Günther: [I&apos;ve released version 0.0.6 of livi - the little video player for mobile: https://gitlab.gnome.org/guidog/livi/-/releases/v0.0.6While this also adds a bunch of new features like language and subtitle selection it mostly moved things closer to the designs. We have a wide mode for docked/desktop mode now and autohiding of panels, etc.#phosh #gnome #gtk #video #gstreamer](https://social.librem.one/@agx/111783787422949500)
- Guido Günther: [Same thing in the wide layout for larger screens:](https://social.librem.one/@agx/111755498811404421)
- Guido Günther: [Livi (a little video player for #LinuxMobile) (but it also has a wide mode for convergent use now) will soon remember stream positions so it&apos;s easy to continue watching. Here&apos;s how it looks in #phosh :#gtk](https://social.librem.one/@agx/111755418496844897)

#### Plasma Ecosystem
- Nate Graham: [This week in KDE: auto-save in Dolphin and better fractional scaling](https://pointieststick.com/2024/01/19/this-week-in-kde-auto-save-in-dolphin-and-better-fractional-scaling/)
- KDE Mentorship: [Season Of KDE 2024 Projects](https://mentorship.kde.org/blog/2024-01-15-sok-24-welcome/)
- KDE e.V. News: [KDE e.V. is looking for a project lead and event manager for environmental sustainability project](https://ev.kde.org/2024/01/14/2024-01-14-job-project-lead-event-manager/)
- Volker Krause: [Events in 2024](https://www.volkerkrause.eu/2024/01/20/events-in-2024.html) _Nice list of events!_
- broulik: [On the Road to Plasma 6, Vol. 5](https://blog.broulik.de/2024/01/on-the-road-to-plasma-6-vol-5/)
- jbb: [New countries in KDE Itinerary](https://jbb.ghsq.de/2024/01/14/New-Itinerary-backends.html). _The Power of Open Source, Ladies, Furries and Gentlemen!_
- Phoronix: [KDE Config File Lookups Now 13~16% Faster, More Fixes Ahead Of KDE Plasma 6.0](https://www.phoronix.com/news/KDE-Plasma-6-Next-Month)

#### Sailfish OS
- Sailfish OS Forum: [Sailfish Community News, 18th January - Community Event](https://forum.sailfishos.org/t/sailfish-community-news-18th-january-community-event/17765)
- flypig's Gecko log: [Day 144](https://www.flypig.co.uk/list)
- flypig's Gecko log: [Day 143](https://www.flypig.co.uk/list)
- flypig's Gecko log: [Day 142](https://www.flypig.co.uk/list)
- flypig's Gecko log: [Day 141](https://www.flypig.co.uk/list)
- flypig's Gecko log: [Day 140](https://www.flypig.co.uk/list)

#### Ubuntu Touch
- Ubuntu Touch Forums News: [Today Is Q&amp;A Day](https://forums.ubports.com/topic/9760/today-is-q-a-day)
- Ubuntu Touch Forums News: [Ubuntu Touch Q&amp;A 132 Call for Questions](https://forums.ubports.com/topic/9743/ubuntu-touch-q-a-132-call-for-questions)
- fredldotme: [Tide is getting more UX adjustments, like this colorful representation of the current status.](https://mastodon.social/@fredldotme/111790773843612307)
- fredldotme: [This is big: Building WebAssembly projects using CMake in TideTap into the whole ecosystem of great libraries!](https://mastodon.social/@fredldotme/111767715944429874)
- fredldotme: [Tide running a WebAssembly &quot;Hello World&quot; the first time around on Linux. Stil a few hiccups here and there but overall looks very promising.](https://mastodon.social/@fredldotme/111756741616918904)
- Tide Tablet IDE  on twitter: [Creating Linux apps using Snapcraft, Creating Ubuntu Touch apps using Clickable, and CMake-based WebAssembly projects.
All from an IDE that is also touch friendly.
More to come soon!](https://nitter.1d4.us/TideTabletIDE/status/1747718276036350449)
- Tide Tablet IDE on twitter: [: Tide running on Ubuntu Touch as a Snap](https://nitter.1d4.us/TideTabletIDE/status/1746660224373518395)

#### Distributions
- Mobian Blog: [Highlights of 2023](https://blog.mobian.org/posts/2024/01/08/highlights-of-2023/)
- Mobian (Mastodon): [If you are not happy with your on-screen-keyboard (#squeekboard) on Mobian, you might want to try out #phosh-osk-stub which is @agx's toy keyboard. I like the layout a lot, apparently it even features local text prediction (have not tried that yet).Remember, it is a toy, so hold back on your support requests.Installation TL;DR: apt install phosh-osk-stub &amp;&amp; update-alternatives --config Phosh-OSKP.S. The screenshot is using PureOS rather than Mobian, but it is available there just as well](https://fosstodon.org/@mobian/111759239605102769)
- Mobian (Mastodon): [We take stock of some of the important things that happened in Mobian-land in #2023.Highlights of 2023 blog post:https://blog.mobian.org/posts/2024/01/08/highlights-of-2023/](https://fosstodon.org/@mobian/111756437510270696)
- postmarketOS Blog: [Moving pmbootstrap](https://postmarketos.org/blog/2024/01/17/moving-pmbootstrap/)
- postmarketOS (Mastodon): [It's time for another round of &quot;who wants to maintain a Pine64 device&quot;on todays episode, the PineBook Pro. Currently a community category device, but it seems that won't be for much longer as it's now sans-maintainer.Please do drop a comment if you're interested in keeping this port in Community, otherwise it will drop back to testing and no longer supported by postmarketOS stable releases.https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4734](https://fosstodon.org/@postmarketOS/111773671755905424)
- postmarketOS (Mastodon): [pmbootstrap's source code has been moved:https://postmarketos.org/blog/2024/01/17/moving-pmbootstrap/As the blog post says, thanks to everybody for their input and sorry for surely disappointing some of the people reading this. It was not at all an easy decision and we are in the process of finding an alternative to gitlab.com for all of pmOS code. Feel free to comment below, but as always we ask of you to keep a friendly tone and to not hate on any of the forges or contribution workflows. Thanks!](https://fosstodon.org/@postmarketOS/111768227435694272)
- postmarketOS (Mastodon): [Folks owning a Nvidia Tegra 2/3/4 device, you might be interested in Svyatoslav Ryhel's work of replacing the proprietary bootloader ✨&gt; Svyatoslav ported most of these devices without owning one, just relying on testing from people. This means anyone having such a device can reach him and eventually will be able to replace the old proprietary vendor bootloader with U-Boot.https://postmarketos.org/blog/2024/01/13/call-for-tegra-u-boot-testers/Thanks to IonAgorria for the photo!#nvidia #tegra #uboot #freesoftware #linux #bootloader](https://fosstodon.org/@postmarketOS/111761104776966137)
- postmarketOS Wiki New Pages: [Lenovo Yoga c630 (lenovo-yoga-c630)](https://wiki.postmarketos.org/wiki/Lenovo_Yoga_c630_(lenovo-yoga-c630))
- Lemmy - postmarketOS: [Bounty for SDM845 camera support](https://lemmy.ml/post/10797325)

#### Apps
- LinuxPhoneApps.org: Apps: [Key Rack](https://linuxphoneapps.org/apps/app.drey.keyrack/)

#### Kernel
- phone-devel: [[PATCH v3 0/3] Kinetic ExpressWire library and KTD2801 backlight driver](http://lore.kernel.org/phone-devel/20240120-ktd2801-v3-0-fe2cbafffb21@skole.hr/)
- phone-devel: [[PATCH v2 0/3] Imagis touch keys and FIELD_GET cleanup](http://lore.kernel.org/phone-devel/20240120-b4-imagis-keys-v2-0-d7fc16f2e106@skole.hr/)
- phone-devel: [[PATCH v4 0/5] input/touchscreen: imagis: add support for IST3032C](http://lore.kernel.org/phone-devel/20240120191940.3631-1-karelb@gimli.ms.mff.cuni.cz/)
- phone-devel: [Front camera on pinephone](https://lore.kernel.org/phone-devel/ZaY44AHISMIh8fHM@duo.ucw.cz/) 
- Phoronix: [RISC-V With Linux 6.8 Restores XIP Kernel Support](https://www.phoronix.com/news/RISC-V-Linux-6.8)
- Phoronix: [Linux 6.8 Adds Input Driver For Adafruit Seesaw Gamepad](https://www.phoronix.com/news/Linux-6.8-Adafruit-Seesaw)
- Phoronix: [Linux 6.8 Merges Fix For Recent Performance Regression Spotted By Linus Torvalds](https://www.phoronix.com/news/Linux-6.8-Regression-Fix)
- Phoronix: [Linux 6.8-rc1 Should Release On Schedule Tomorrow](https://www.phoronix.com/news/Linux-6.8-rc1-Tomorrow)
- Phoronix: [Linux 6.8 Default-Disabling 31-bit Enterprise System Architecture ELF Binary Support](https://www.phoronix.com/news/Linux-6.8-s390-Disable-ESA-31b)
- Phoronix: [Linus Torvalds Gets Back To Merging New Code For Linux 6.8](https://www.phoronix.com/news/Linux-6.8-Merges-Resume)
- Purism: [Purism and Linux 6.5 to 6.7](https://puri.sm/posts/purism-and-linux-6-5-to-6-7/)

#### Non-Linux
- Phoronix: [Genode OS Aiming For Multi-Monitor & Suspend/Resume Support This Year](https://www.phoronix.com/news/Genode-2024-Plans)

#### Stack
- Phoronix: [Mesa Vulkan Drivers Reach An Inflection Point: Idea Raised To Be More Like Gallium3D](https://www.phoronix.com/news/Mesa-Vulkan-Driver-Runtime)
- Phoronix: [FreeRDP 3.2 Fixes Wayland Client Scaling + Wayland Keyboard Handling Fixes](https://www.phoronix.com/news/FreeRDP-3.2-Released)
- Phoronix: [SDL 2.30 Release Candidate Brings Many Fixes, New Additions For The Steam API](https://www.phoronix.com/news/SDL-2.30-RC1-Released)
- Phoronix: [Wayland Protocols 1.33 Released With DMA-BUF Stable, Adds Transient Seat Protocol](https://www.phoronix.com/news/Wayland-Protocols-1.33)
- Phoronix: [Mesa 24.0-rc2 Released With This Quarter's Release Looking Good](https://www.phoronix.com/news/Mesa-24.0-rc2-Released)
- Phoronix: [Servo Browser Engine Making Embedded App Progress With Tauri](https://www.phoronix.com/news/Servo-Engine-Plus-Tauri)
- Phoronix: [Hangover Aiming For RISC-V Support This Year, x86_64 Emulation](https://www.phoronix.com/news/Hangover-9.0-Released)

#### Matrix
- Matrix.org: [This Week in Matrix 2024-01-19](https://matrix.org/blog/2024/01/19/this-week-in-matrix-2024-01-19/)

### Worth Noting
- [caleb: "app drawer go wheeee…"](https://social.treehouse.systems/@cas/111757469313679817)
- [Abel Vesa: "2024 is definitely going to be the year of Linux on arm-based laptops"](https://social.kernel.org/notice/AduRTk0B7j03rYJ69I)
- [pabloyoyoista: " recently got my Shift6mq from @shiftphones back from repairs, and the power issues I was experimenting before seem to be gone.…"](https://social.treehouse.systems/@pabloyoyoista/111768023452237830)
- [minute: "suspend to ram / resume works now on MNT Pocket Reform (with i.MX8MPlus). …"](https://mastodon.social/@mntmn/111765997261914094)
- [Alexandre Franke: "#Fractal 6 is up and ready! …"](https://mamot.fr/@afranke/111778246625371283)
- [Ruben De Smet: "Some very necessary bugfixes and features were added to #Whisperfish again!…"](https://mastodon.social/@rubdos/111776442075543057) _Whisperfish is a Sailfish OS Signal client._
- [caleb: "what kind of devices and what usecases would you be interested in using generic ARM64 #postmarketOS images for?…"](https://social.treehouse.systems/@cas/111773529128482146)
- [Markus Göllnitz: "Railway just received a new set of labels, tooltips, and text. Now with better, more precise wording, and that thanks to Sydney Sharpe, as she provided me with a lot of helpful input.…"](https://floss.social/@camelCaseNick/111793836369261802)
- [Fiona: "Full success! I'm sending this post from my PinePhone, over wifi with rtw8723cs! I have a working connection with WPA3. …" - queer.af](https://queer.af/@airtower/111791990795808180). _THIS. IS. A M A Z I N G !_
- [Mike Sheldon: "I've just released version 0.2 of Pied (https://pied.mikeasoft.com), my tool for making advanced text-to-speech voices work easily on Linux.…"](https://octodon.social/@mikesheldon/111790016542019355)
- [Robert Mader: "After some more debugging, her…"](https://floss.social/@rmader/111784608234332676)
- [Martijn Braam: "I've now also released an initial release of libdng…"](https://fosstodon.org/@martijnbraam/111783922282846919)
- Jozef Mlich on Fosstodon: [You can compare #telescope and #amazfish how it works with #AsteroidOS  https://www.youtube.com/shorts/8pWQv5yHQ80https://www.youtube.com/shorts/3takNZWmBIA](https://fosstodon.org/@jmlich/111783422328587960)
- PinePhone (Reddit): [Several problems with Pinephone Pro (failing to boot despite bootable eMMC installation, screen turns off briefly after logging in, and even if the phone gets laid on a table). Did I possibly receive a faulty device?](https://www.reddit.com/r/pinephone/comments/198cnqs/several_problems_with_pinephone_pro_failing_to/)
- PinePhone (Reddit): [Nethunter on PinePhone Pro: lsusb shows no devices](https://www.reddit.com/r/pinephone/comments/197cin9/nethunter_on_pinephone_pro_lsusb_shows_no_devices/)
- PINE64official (Reddit): [Pinephone Keyboard Pine button not working](https://www.reddit.com/r/PINE64official/comments/19bk1kj/pinephone_keyboard_pine_button_not_working/)
- PINE64official (Reddit): [Where are the Pine64 updates?](https://www.reddit.com/r/PINE64official/comments/199zn2x/where_are_the_pine64_updates/)
- PINE64official (Reddit): [Looking to buy a pine note](https://www.reddit.com/r/PINE64official/comments/198s3n4/looking_to_buy_a_pine_note/)
- Purism (Reddit): [Hey, all, I've updated the settings for this Sub to encourage more skepticism from potential Purism customers…](https://www.reddit.com/r/Purism/comments/196olm5/hey_all_ive_updated_the_settings_for_this_sub_to/). _I have been calling that subreddit r/purismhatefest for ages._
- Purism community: [Is GNOME slowing our phones down and becoming a liability?](https://forums.puri.sm/t/is-gnome-slowing-our-phones-down-and-becoming-a-liability/22420). _Oof._
- Purism community: [Incoming calls in Germany - O2 -> no with vodafone -> YES](https://forums.puri.sm/t/incoming-calls-in-germany-o2-no-with-vodafone-yes/22418)
- Purism community: [Librem 5 works great now!](https://forums.puri.sm/t/librem-5-works-great-now/22405)
- Purism community: [Battery hacking - electrical engineers care to comment?](https://forums.puri.sm/t/battery-hacking-electrical-engineers-care-to-comment/22404)
- Purism community: [How to bring up OSK in script?](https://forums.puri.sm/t/how-to-bring-up-osk-in-script/22397)
- Lemmy - linuxphones: [Call For Tegra U-Boot Testers](https://postmarketos.org/blog/2024/01/13/call-for-tegra-u-boot-testers/)

### Worth Reading
- Mobian Blog: [Highlights of 2023](https://blog.mobian.org/posts/2024/01/08/highlights-of-2023/)
  - r/MobileLinux: [Mobian Highlights of 2023](https://www.reddit.com/r/mobilelinux/comments/196w74b/mobian_highlights_of_2023/)
- Martijn Braam: [The dilemma of tagging library releases](https://blog.brixit.nl/the-dilemma-of-tagging-library-releases/)
- Neil Brown: [A week of not using a search engine](https://neilzone.co.uk/2024/01/a-week-of-not-using-a-search-engine/)
- freeyourself: [Quicker Internet Search on PureOS and the Librem 5](https://freeyourself.justlearning.net/0001_quick_search.html)
- Purism: [Purism and Linux 6.5 to 6.7](https://puri.sm/posts/purism-and-linux-6-5-to-6-7/)
- Christian Gmeiner: [The Year 2023 in Retrospect](https://christian-gmeiner.info/2022-12-26-end-of-year/). _A post on etnaviv progress (Librem 5 GPU driver)._

### Worth Watching
- UBports: [Ubuntu Touch Q&A 132](https://www.youtube.com/watch?v=6_PVkeMPgLk)
- vlogize: [Unlocking Possibilities: Porting Ubuntu Touch to Unsupported Tablets](https://www.youtube.com/watch?v=LjYf1TE-K-E)
- DORS/CLUC conference: [DORS/CLUC 2023 Merlijn Wajer – Maemo Leste A Debian/Devuan based mobile hacker OS](https://www.youtube.com/watch?v=qZmYBex98xQ)

_The video section is shorter than usual, as the way to [conveniently aggregate YouTube videos by using a searX instance](https://framagit.org/linmob/linmob.frama.io/-/blob/main/utils/aggregate.py?ref_type=heads#L181-215), that allows RSS feeds, is currently unavailable, as the the searx instance is down. I was unable to quickly find another searX(NG) instance that supports this. **Help with finding another [searX(NG) instance](https://searx.space/) supporting RSS feeds or with the [collection of relevant videos](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit#) is highly appreciated!**_

### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

