+++
title = "Weekly GNU-like Mobile Linux Update (36/2023): Phosh 0.31.0 and a postmarketOS Service Pack"
date = "2023-09-10T19:07:55Z"
draft =  false
[taxonomies]
tags = ["Sailfish OS", "Ubuntu Touch", "Nemo Mobile", "Mobile NixOS", "Capyloon", "Phosh", "Librem 5",]
categories = ["weekly update"]
authors = ["Peter",]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

postmarketOS 23.06.1 brings updates to devices supported by the stable branch, Nemo Mobile has Qt6 based packages, Capyloon show of new features and Servo running on the PinePhone Pro, more Gecko porting to Sailfish OS, Librem 5 shipping updates, Beepy charge mods and more!

<!-- more -->
_Commentary in italics._

### Hardware options
- [Denzil Ferreira 🌳🍄: "We managed to crack Hisense A9 bootloader lock, and root over at XDA brainpower.…"](https://techhub.social/@denzilferreira/111031099804718979)

### Software

#### Gnome Ecosystem
- This Week in GNOME: [#112 New Circle Apps](https://thisweek.gnome.org/posts/2023/09/twig-112/)
- xjuan: [Cambalache 0.14.0 Released!](https://blogs.gnome.org/xjuan/2023/09/07/cambalache-0-14-0-released/)
- The Open Sourcerer: [Help us make GNOME Calendar rock-solid by expanding the test suite!](https://fortintam.com/blog/call-for-help-writing-gnome-calendar-compliance-unit-tests/)
- jrb: [Crosswords 0.3.11: Acrostic Panels](https://blogs.gnome.org/jrb/2023/09/03/crosswords-0-3-11-acrostic-panels/)
- Phosh.mobi: [Phosh 0.31.0](https://phosh.mobi/releases/rel-0.31.0/)
- Guido Günther: [People had different theories why phosh-osk-stub woudn&apos;t use the whole display width on wider displays so I was buckling up for a full afternoon of debugging sizing issues but as it turns out after a couple of minutes we were just missing a tightening-threshold in a HdyClamp: Rest of the afternoon saved thanks to #gtk inspector 🎉 #phosh #linuxmobile](https://social.librem.one/@agx/111030139864351381)
- Phoronix: [GNOME 45.rc Brings GDM Wayland Multi-Seat, More libadwaita Adoption](https://www.phoronix.com/news/GNOME-45-rc)

#### Plasma Ecosystem
- Nate Graham: [This week in KDE: power management galore](https://pointieststick.com/2023/09/08/this-week-in-kde-power-management-galore/)
- Nate Graham: [September Plasma 6 update](https://pointieststick.com/2023/09/06/september-plasma-6-update/)
- KDE Announcements: [KDE Ships Frameworks 5.110.0](https://kde.org/announcements/frameworks/5/5.110.0/)
- bobulate: [Calamares Qt6](https://euroquis.nl//kde/2023/09/09/calamares.html)
- Carl Schwan: [Francis 1.0](https://carlschwan.eu/2023/09/08/francis-1.0/)
- Christoph Cullmann: [KDE Frameworks 6 / Plasma / Gear Release Schedule Plan](https://cullmann.io/posts/kf6-release-plan/)
- dot.kde.org: [Akademy 2024 Call for Hosts](https://dot.kde.org/2023/09/06/akademy-2024-call-hosts)

#### Sailfish OS
- Community News - Sailfish OS Forum: [Sailfish Community News, 7th Sep, Romania Meetup](https://forum.sailfishos.org/t/sailfish-community-news-7th-sep-romania-meetup/16587)
- adampigg: [#SailfishOS on the @PINE64 #PinephonePro kernel is updated to Megi's 6.5.2, which fixes usb-networking and the very annoying panic on usb disconnect!](https://fosstodon.org/@piggz/111028459677935621)
- flypig's Gecko log: [Day 24](https://www.flypig.co.uk/list?list_id=863&list=gecko)
- flypig's Gecko log: [Day 23](https://www.flypig.co.uk/list?list_id=862&list=gecko)
- flypig's Gecko log: [Day 22](https://www.flypig.co.uk/list?list_id=861&list=gecko)
- flypig's Gecko log: [Day 21](https://www.flypig.co.uk/list?list_id=860&list=gecko)
- flypig's Gecko log: [Day 20](https://www.flypig.co.uk/list?list_id=859&list=gecko)

#### Ubuntu Touch
- Ubuntu Touch Forums News: [FOSDEM 2024](https://forums.ubports.com/topic/9339/fosdem-2024)
- UBports - Mastodon: [With #UBports #UbuntuTouch, #convergence is not a dream!https://www.youtube.com/shorts/e1NSOznvLIc#Ubuntu #Linux #LinuxMobile #OpenSource](https://mastodon.social/@ubports/111012694631375519)
- UBports - Mastodon: [#UBports #App #UbuntuTouch #Convergence #LinuxMobile #Linux #opensource Sapot Browser available on the #OpenStore https://youtube.com/shorts/Ro8UiXxIHro?si=9qToxXf4x_uup6x1](https://mastodon.social/@ubports/111002958870009982)
- fredldotme: [Cannot wait to give my talk at the Ubuntu Summit 2023! Want to keep it as a surprise for now but rest assured, it'll be awesome.](https://mastodon.social/@fredldotme/111012585965490101)
- Jozef Mlich on Fosstodon: [I have some progress in porting of Amazfish to @ubports .](https://fosstodon.org/@jmlich/111019477485054955)
- Jozef Mlich on Fosstodon: [I have new @PINE64 PineTime, thanks to @JF  I have started with testing of  @piggz Amazfish with device.I have no clue how why bluetooth pairing doesn't work still, but I have fixed loading of translations ;-DI will be happy if someone helps with debugging of that https://github.com/piggz/harbour-amazfish/pull/265](https://fosstodon.org/@jmlich/111013909872090232)

#### Nemo Mobile
- [neochapay (@neochapay): "YEA! Qt6 #nemomobile #glacierUX build on real device! #pinephone @thepine64 repo is here https://img.nemomobile.net/manjaro/10.2023/devel/aarch64/" | nitter](https://nitter.net/neochapay/status/1698752201336721615)

#### Capyloon
- [Capyloon: "Friday hack: getting @servo to run on a Pinephone Pro! (some patches are needed, where would be the fun without that?)…"](https://fosstodon.org/@capyloon/111032248243803371)
- [Capyloon: "You can now get some information at a glance by just tilting your device: time, battery level, pending notifications.…"](https://fosstodon.org/@capyloon/111019544887470852)

#### Distributions
- postmarketOS Blog: [v23.06 SP1: Through Staging](https://postmarketos.org/blog/2023/09/10/v23.06-sp1/)
- Phoronix: [LoongArch With Linux 6.6 Adds KGDB/KDB, KFence, KASAN, LBT Binary Translation](https://www.phoronix.com/news/Linux-6.6-LoongArch)

#### Apps
- Purism forums: List of Apps that fit and function well: [The latest Signal flatpak works in portrait mode now.](https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/368)

#### Kernel
- phone-devel: [[PATCH v3 0/2] leds: Add a driver for KTD202x](http://lore.kernel.org/phone-devel/20230906-ktd202x-v3-0-7fcb91c65d3a@apitzsch.eu/)
- phone-devel: [Re: [PATCH 04/11] arm64: dts: qcom: pm7250b: make SID configurable](http://lore.kernel.org/phone-devel/CVAUDGBO4S08.1F0O66ZE6I4IG@otso/)
- phone-devel: [Re: [PATCH 11/11] arm64: dts: qcom: qcm6490: Add device-tree for Fairphone 5](http://lore.kernel.org/phone-devel/CV9ZIJFTEBQE.1035ODUQD2B78@otso/)
- phone-devel: [Re: [PATCH v2 1/2] dt-bindings: leds: Add Kinetic KTD2026/2027 LED](http://lore.kernel.org/phone-devel/1b086e32-fa1f-815f-f326-ef4d13f6fbc6@linaro.org/)
- lowendlibre: [MediaTek Mainlining Madness](http://lowendlibre.github.io/posts/mediatek-mainlining-madness/)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-09-08](https://matrix.org/blog/2023/09/08/this-week-in-matrix-2023-09-08/)
- Matrix.org: [Welcoming Josh Simmons as Managing Director of the Matrix.org Foundation!](https://matrix.org/blog/2023/09/introducing-josh-simmons-managing-director/)

#### Releases
- Phoronix: [FreeBSD 14 Beta Released - Initial WiFi 6 Support, Updated LLVM Toolchain, Fwget Utility](https://www.phoronix.com/news/FreeBSD-14-Beta-1)
- Phoronix: [KDE Frameworks 6 & Qt6'ed Gear Apps Will Release Alongside Plasma 6.0 In February](https://www.phoronix.com/news/KDE-KF6-Qt6-Gear-February)
- Phoronix: [KDE Plasma 6.0 Planned For Release In Early February](https://www.phoronix.com/news/KDE-Plasma-6.0-February)

### Worth Noting
- [FOSDEM: "FOSDEM 2024 dates: 3 & 4 February 2024…"](https://fosstodon.org/@fosdem/111003807844280540)
- [@matthewcroughan: "Super Mario 64 (sm64ex) running from a phone natively on Mobile #NixOS via GUD (Generic USB Display) Linux 6.4.0"](https://social.defenestrate.it/notice/AZOIU17ea3lpH0Wvzs)
- [justsoup: "Alright. I've decided I am actually going to do it. I am going to make Void Linux Mobile.…"](https://mstdn.social/@justsoup/111015720087982597)
- [NGI Zero open source funding: "Are you working on a project that makes the internet better for everyone?…"](https://mastodon.xyz/@NGIZero/110972744577387708)
- Lemmy - linuxphones: [Samsung Dex Station with PinePhone Pro](https://lemmy.ml/pictrs/image/ca5d808f-b496-43fb-992c-7329efc3b0f7.jpeg)
- Lemmy - linuxphones: [How powerful is Pinephone Pro?](https://lemmy.ml/post/4459201)
- Lemmy - linuxphones: [Finished Chip 'n Dale Rescue Rangers](https://lemmy.ml/pictrs/image/ec416174-f031-422c-967b-3a23ca0f9e8c.jpeg)
- PINE64official (Reddit): [Have I bricked my PinePhone Pro Explorer Edition? (purchased before July 2022)](https://www.reddit.com/r/PINE64official/comments/16dmx44/have_i_bricked_my_pinephone_pro_explorer_edition/)
- PINE64official (Reddit): [PineNote and Zotero](https://www.reddit.com/r/PINE64official/comments/16btm37/pinenote_and_zotero/)
- Purism (Reddit): [I don't know whether to laugh or cry.....](https://www.reddit.com/r/Purism/comments/16cxl5n/i_dont_know_whether_to_laugh_or_cry/)
- Purism (Reddit): [Waiting for a refund? Class-action lawsuit](https://www.reddit.com/r/Purism/comments/16cn3of/waiting_for_a_refund_classaction_lawsuit/)
- Purism (Reddit): [What kernel do you have installed on your Android/AOSP phone?](https://www.reddit.com/r/Purism/comments/169a2kw/what_kernel_do_you_have_installed_on_your/)
- Purism community: [Signal app now usable in portrait mode on L5](https://forums.puri.sm/t/signal-app-now-usable-in-portrait-mode-on-l5/21278)
- Purism community: [[Guide] How to investigate and fix a Librem 5 installation that hangs at boot](https://forums.puri.sm/t/guide-how-to-investigate-and-fix-a-librem-5-installation-that-hangs-at-boot/21276)
- Purism community: [When is Crimson coming?](https://forums.puri.sm/t/when-is-crimson-coming/21268)
- [Keywan Tonekaboni: "Eine Recherche, wo ich großen Spaß hatte, angefangen bei einem sehr spannenden Interview mit @ollieparanoid bis hinzu freudiges herum experimentieren auf Vintage-Geräten: @postmarketOS ausprobiert und Hintergründe zum Projekt und der Entstehungsgeschichte.…" - Heise Medien on Mastodon](https://social.heise.de/@ktn/111023328543158863)
- [Mobian: "An article on @mobian on phone…"](https://fosstodon.org/@mobian/111017999338153580)

### Worth Reading
- M0YNG.uk [Beepy charge mods for infinite battery life](https://m0yng.uk/2023/09/Beepy-charge-mods-for-infinite-battery-life/)
- lowendlibre: [MediaTek Mainlining Madness](http://lowendlibre.github.io/posts/mediatek-mainlining-madness/)
- Purism: [PureOS on Liberty Phone and Librem 5](https://puri.sm/posts/pureos-on-liberty-phone-and-librem-5/)
  - Purism community: [New Post: PureOS on Liberty Phone and Librem 5](https://forums.puri.sm/t/new-post-pureos-on-liberty-phone-and-librem-5/21269)
- Purism: [All Librem 5 Smartphones Have Shipped!](https://puri.sm/posts/all-librem-5-smartphones-have-shipped/)

### Worth Watching
- Purism: [PureOS on Liberty Phone and Librem 5](https://www.youtube.com/watch?v=Gb1odRmZKzI)
- Continuum Gaming: [Continuum Gaming E381: Browser Dev SFOS Browser & Firefox ESR](https://www.youtube.com/watch?v=1B3SNjAttrM)
- UBports: [UBports Q&A 123](https://www.youtube.com/watch?v=tedLPBTDxbU)
- Linux Stuff: [OnePlus 6 (Enchilada) - Droidian - Camera](https://www.youtube.com/watch?v=eUU2cIvc2_M)
- NixCon: [NixCon2023 Daily-driving NixOS on the Librem 5](https://www.youtube.com/watch?v=unR0XGd9bxM)
- Ivo Xavier: [Fairphone 4 with Ubuntu Touch OTA-3: Performance Boost](https://www.youtube.com/watch?v=I_J8Zfsrfc8)


### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__


