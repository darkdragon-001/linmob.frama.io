+++
title = "Weekly GNU-like Mobile Linux Update (52/2023): Closing remarks for 2023"
date = "2023-12-30T21:58:31Z"
draft = false
[taxonomies]
tags = ["Sailfish OS", "Ubuntu Touch", "Asteroid OS", "mutt", "37c3", "conferences",]
categories = ["weekly update"]
authors = ["Peter",]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

A quiet, last week of the year, collected for the last time in 2023. Thank you for reading the updates in the past year, and all the kind feedback. :-) 
<!-- more -->

Personally, 37c3 was a great ending to a mixed year, despite the assembly issues[^1]. See you next year!

_AI Summary:_ This post covers recent updates and developments in the Linux and open-source mobile ecosystem. It includes GNOME and Plasma ecosystem updates, developments in Sailfish OS, new additions to Ubuntu Touch, advancements in various Linux distributions, app releases, kernel patches, updates in the software stack and Matrix, and several noteworthy readings, videos, and community discussions related to mobile Linux.

_Commentary in italics._

### Software

#### Gnome Ecosystem
- This Week in GNOME: [#128 Bye Bye 2023](https://thisweek.gnome.org/posts/2023/12/twig-128/)

#### Plasma Ecosystem
- Nate Graham: [Does Wayland really break everything?](https://pointieststick.com/2023/12/26/does-wayland-really-break-everything/)
- Phoronix: [KDE KWin Merge Request Opened For Dynamic Triple Buffering](https://www.phoronix.com/news/KDE-KWin-Triple-Buffering-MR)

#### Sailfish OS
- flypig's Gecko log: [Day 122](https://www.flypig.co.uk/list?list_id=969&list=gecko)
- flypig's Gecko log: [Day 121](https://www.flypig.co.uk/list?list_id=968&list=gecko)
- flypig's Gecko log: [Day 120](https://www.flypig.co.uk/list?list_id=967&list=gecko)
- flypig's Gecko log: [Day 119](https://www.flypig.co.uk/list?list_id=966&list=gecko)
- flypig's Gecko log: [Day 118](https://www.flypig.co.uk/list?list_id=965&list=gecko)

#### Ubuntu Touch
- fredldotme on twitter: [Please help a motherfucker eat and spread the word!](https://nitter.1d4.us/fredldotme/status/1740001392318890433#m) _Alfred does great work, please support him if you can!_
- rubenlcarneiro on twitter: [Welcome to OnePlus Noed N100 to @Ubports and  #UbuntuTouch family. https://gitlab.com/ubports/porting/community-ports/android10/oneplus-nord-n100. Also check the README file to Intructions and if you do like my work support it.
](https://nitter.1d4.us/rubenlcarneiro/status/1740816555662049555#m)
- Phoronix: [Ubuntu Touch OTA-3 Focal Brings PinePhone Images, Initial Snap Support](https://www.phoronix.com/news/Ubuntu-Touch-OTA-3-Focal)

#### Distributions
- Droidian (Mastodon): [Upcoming 📣5G data support, Latest Mobile Camera Tech in Linux, Modernizing Modem, Even faaaaaster App launches, Modern devices (ThinkPhone and Pixel 6) and as always Improved UI! Join the fun at Droidian side ;)#LinuxMobile #LinuxCamera #Privacy #Linux #Droidian #Phosh #Gnome #OpenSource #Mobile #FreeSoftware #LinuxSmartPhones #LinuxOnMobile](https://fosstodon.org/@droidian/111661772943631904)
- postmarketOS pmaports Merge Requests: [Draft: SDM845 generic device support](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4599)
- postmarketOS Wiki New Pages: [Openbox](https://wiki.postmarketos.org/wiki/Openbox)
- Phoronix: [Gentoo Ends Out 2023 By Offering Up Binary Packages For Direct Installation](https://www.phoronix.com/news/Gentoo-Binaries-EOY-2023)
- Phoronix: [Fedora 40 Plans To Unify /usr/bin & /usr/sbin](https://www.phoronix.com/news/Fedora-40-Unify-usr-bin-sbin)

#### Apps
- LinuxPhoneApps.org: Apps: [Televido](https://linuxphoneapps.org/apps/de.k_bo.televido/)
- LinuxPhoneApps.org: Apps: [KWordQuiz](https://linuxphoneapps.org/apps/org.kde.kwordquiz/)
- LinuxPhoneApps.org: Apps: [Wifi Connect](https://linuxphoneapps.org/apps/noappid.andym48.pinephone-wifi/). _Thank you, andym48!_
- Guido Günther: [I&apos;ve released #livi 0.0.5, a little video player mostly targeting mobile: https://gitlab.gnome.org/guidog/liviAmong other things we make better use of screen estate in landscape even when not fullscreen. #phosh #gnome #linuxmobile](https://social.librem.one/@agx/111641517503871333)
- [Hemish/हेमिश/ਹੇਮਿਸ਼/ہیمِش: "Announcing Crusher: A fun app …"](https://mastodon.world/@hemish/111670714662588680)
- [Schmiddi on Mobile: "Version 0.11.0 of Flare brings…"](https://fosstodon.org/@schmiddionmobile/111645722962487596)

#### Kernel
- phone-devel: [[PATCH] arm64: dts: qcom: qcm6490-fairphone-fp5: Add missing reserved-memory](http://lore.kernel.org/phone-devel/20231229-fp5-reserved-mem-v1-1-87bb818f1397@fairphone.com/)
- phone-devel: [[PATCH] arm64: dts: qcom: sc7280: Add static properties to cryptobam](http://lore.kernel.org/phone-devel/20231229-sc7280-cryptobam-fixup-v1-1-bd8f68589b80@fairphone.com/)
- phone-devel: [[RFC PATCH 0/5] regulator: support for Marvell 88PM886 LDOs and bucks](http://lore.kernel.org/phone-devel/20231228100208.2932-1-karelb@gimli.ms.mff.cuni.cz/)
- phone-devel: [[PATCH v3 0/8] Add UFS support for SC7180/SM7125](http://lore.kernel.org/phone-devel/20231225120327.166160-1-davidwronek@gmail.com/)

#### Stack 
- Phoronix: [Mesa 23.3.2 Released With Plenty Of Bug Fixes For Closing Out 2023](https://www.phoronix.com/news/Mesa-23.3.2-Released)

#### Matrix
- Matrix.org: [The Matrix Holiday Update 2023](https://matrix.org/blog/2023/12/25/the-matrix-holiday-update-2023/)

### Worth Noting
- Jozef Mlich on Fosstodon: [Hello world, your notification is here. #Amazfish implementation of #AsteroisOS notification service.](https://fosstodon.org/@jmlich/111641405711402505)
- [Enron Hubbard: "I seriously can’t understate how important all of the work on #postmarketOS is to the #solarpunk future. …"](https://tilde.zone/@enron/111666674425323837)
- [# [Mobile-Friendly-Firefox](https://codeberg.org/user0/Mobile-Friendly-Firefox)...](https://fedia.io/m/linuxphones@kbin.social/p/278635)
- [postmarketOS New Devices Bot: "#postmarketOS now boots on generic x86_64 system!…" - Mastodon](https://mastodon.social/@pmOS_devices/111665479978452874)
- [caleb: "Have YOU seen the Linux on Mobile assembly?…" - Treehouse Mastodon](https://social.treehouse.systems/@cas/111663777045682651)
- [PocketVJ: "#droidian on the #op6 gives me…"](https://fosstodon.org/@pocketvj/111651530896352705)
- [Martijn Braam: "Good 'ol 2019 #postmarketos on the pinephone development hardware…"](https://fosstodon.org/@martijnbraam/111654900987105693)
- [Martijn Braam: "I made a simple APKBUILD formatter. It does not contain nearly enough rules in it yet to make everything perfect but it's a start :)…"](https://fosstodon.org/@martijnbraam/111652924593681866)
- r/MobileLinux: [Phosh v0.34.0 Released!](https://www.reddit.com/r/mobilelinux/comments/18twu5y/phosh_v0340_released/)
- r/MobileLinux: [postmarketOS v23.12: The One We Asked The Community To Name](https://www.reddit.com/r/mobilelinux/comments/18tvs0a/postmarketos_v2312_the_one_we_asked_the_community/)
- r/MobileLinux: [Newest phone/most powerful phone that can run linux](https://www.reddit.com/r/mobilelinux/comments/18sflg5/newest_phonemost_powerful_phone_that_can_run_linux/)
- PINE64official (Reddit): [Looking for a upgrade from the PinePhone Community Edition](https://www.reddit.com/r/PINE64official/comments/18qhbup/looking_for_a_upgrade_from_the_pinephone/)
- Purism community: [Wifi Calling with Librem 5](https://forums.puri.sm/t/wifi-calling-with-librem-5/22256)
- Purism community: [L5 dock - flickering convergence display](https://forums.puri.sm/t/l5-dock-flickering-convergence-display/22238)

### Worth Reading
- David Heidelberg: [How to help the Linux mobile community? Pt. 1: Minetest](https://ixit.cz/blog/2023-12-21-Minetest-Linux-touch-pt1) _Nice work, and nice meeting you at 37C3, David!_
- Jan Wagemakers: [Reading my emails with Mutt on a postmarketOS/Sxmo smartphone](https://www.janwagemakers.be/jekyll/postmarketos/sxmo/mutt/2023/12/28/Shift6mq-Mutt.html) _Well done! (I mostly use aerc though.)_
- KDE blogs: [Video Recommendation: Coping with Other People's Code - Laura Savino](https://blogs.kde.org/2023/12/26/video-recommendation-coping-other-peoples-code-laura-savino)

### Worth Watching
- Continuum Gaming: [E377: SFOS App – Captain's Log](https://www.youtube.com/watch?v=KSx0eN7lIQQ)
- Digital Creators Lab: [Pinephone unboxing.](https://www.youtube.com/watch?v=8sxqc568mW0)
- kayuz6: [Ubuntu Touch on Xperia 10](https://www.youtube.com/watch?v=VyF20Z2ICpY)
- AnimaccordFan: [My First Linux Phone - Ubuntu Touch on Samsung Galaxy S7](https://www.youtube.com/watch?v=C2R5KCGgy9A)
- ArtoSeVeN: [Ubuntu Touch Testing Di Xiaomi Mi Mix2s](https://www.youtube.com/watch?v=u7VZ0T7Nsts)

#### 37c3 
_All of these talks are not really too relevant to the topic at hand. I included them to give you an idea of the talks Congress offers, and rest assured, there's way, way more._
- [Turning Chromebooks into regular laptops - media.ccc.de](https://media.ccc.de/v/37c3-11929-turning_chromebooks_into_regular_laptops)
- [Apple's iPhone 15: Under the C - media.ccc.de](https://media.ccc.de/v/37c3-12074-apple_s_iphone_15_under_the_c)
- [Open CPU / SoC design, all the way up to Debian - media.ccc.de](https://media.ccc.de/v/37c3-11777-open_cpu_soc_design_all_the_way_up_to_debian)
- [Writing secure software - media.ccc.de](https://media.ccc.de/v/37c3-11811-writing_secure_software)
- [Breaking "DRM" in Polish trains - media.ccc.de](https://media.ccc.de/v/37c3-12142-breaking_drm_in_polish_trains)
- [YOU’VE JUST BEEN FUCKED BY PSYOPS - media.ccc.de](https://media.ccc.de/v/37c3-12326-you_ve_just_been_fucked_by_psyops)

### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

[^1]: I was not prepared to be told by habitat orga that there would be no table for a Linux on Mobile Assembly, claiming I had not replied to an email (which I had) and could prove. The bad side of being drained and tired is, that you can't really fight back surprise setbacks, and so I caved - sorry again for everybody who got frustrated by being unable to find the assembly!
