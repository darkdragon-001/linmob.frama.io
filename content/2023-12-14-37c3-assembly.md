+++
title = "37C3 Unlocked: Linux on Mobile Assembly"
date = "2023-12-14"
updated = "2023-12-28"
draft = false
[taxonomies]
tags = ["37c3", "Chaos Communication Congress",]
categories = ["events"]
authors = ["Peter",]
[extra]
update_note = "Sad news"
+++

TL;DR: If you attend 37C3 and have a ticket, please visit/join the Linux on Mobile assembly! (If not, see you at FOSDEM!)


<mark>Due to miscommunication and a lack of tables, the assembly is not where it was supposed to be and thus barely exists. See the [assembly page](https://events.ccc.de/congress/2023/hub/en/assembly/linux-on-mobile/) for details!</mark> 

<mark>See also: [https://linmob.net/37c3/](https://linmob.net/37c3/)</mark>
<!-- more -->


The [37th Chaos Communication Congress](https://events.ccc.de/congress/2023/hub/en/index), theme "Unlocked", will take place in Hamburg, Germany, from December 27th to 30th, 2023. It is sold out - however, if you're spontenous, you may be able to get a ticket on the fediverse - there are always people who can't make it. 36C3 happenend in 2019, and then ... you know - I am really excited to attend again.

While my talk (understandably) did not make the cut, the "Linux on Mobile" assembly made it. It's going to be tiny, but I look forward to do meet and talk likeminded people, maybe hack on things, or unlock some bootloaders. The assembly will highlight projects such as postmarketOS and Ubuntu Touch with emphasis on sustainability and fun :)

For more details, see the [page](https://events.ccc.de/congress/2023/hub/en/assembly/linux-on-mobile/) of the assembly.

If you attend 37C3, please get in touch via email or one of the social options below - whether you intend to join the assembly or not!

### Can't make it?

Don't worry. The main talks are going [to be streamed and recorded](https://media.ccc.de/b/congress/2023), and I think [FOSDEM 2024](https://fosdem.org/2024/) is going to have a way better "Mobile Linux per unit of currency" value - see you there!


<mark>See also: [https://linmob.net/37c3/](https://linmob.net/37c3/)</mark>
