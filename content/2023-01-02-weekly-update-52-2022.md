+++
title = "Weekly GNU-like Mobile Linux Update (52/2022): Year In Review and other topics"
date = "2023-01-02T14:15:00Z"
updated = "2023-01-02T15:59:00Z"
draft = false
[taxonomies]
tags = ["Ubuntu Touch","Nemo Mobile","PinePhone Pro","Sailfish OS","NuttX","Phosh","Matrix",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
update_note = "Added links to Aaron's Blog posts in Worth Reading"
+++

A number of round-ups, but also some news.
<!-- more -->

_Commentary in italics._

### Hardware enablement
- Phoronix: [PinePhone Pro Display Support Nearing The Mainline Linux Kernel](https://www.phoronix.com/news/PinePhone-Pro-Display-Support)

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#76 Last Fragments of 2022](https://thisweek.gnome.org/posts/2022/12/twig-76/) _Mobile Money? Exciting!_
- Marcus Lundblad: [Maps wrap-up 2022](http://ml4711.blogspot.com/2022/12/maps-wrap-up-2022.html)
- Sophie Herold: [That was 2022](https://blogs.gnome.org/sophieh/2022/12/26/that-was-2022/)
- [Guido Günther: "phosh 0.23.0 is out 🚀📱 : There's a new lockscreen plugin to show emergency information by @kop316 , Plugins can now have preferences, we switched documentation to gi-docgen and there's more.…"](https://social.librem.one/@agx/109592156900939697)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: end-of-year goodies](https://pointieststick.com/2022/12/30/this-week-in-kde-end-of-year-goodies/)
- MauiKit blog: [Maui Report 20](https://mauikit.org/blog/maui-report-20/)

#### Nemo Mobile
- Nemo Mobile UX team: [Nemomobile in December 2022](https://nemomobile.net/pages/nemomobile-in-december-2022/)
- [Jozef Mlich: "I have tried to rebuild my own compose of #NemoMobile.…"](https://fosstodon.org/@jmlich/109601438041579061)

#### Ubuntu Touch
- [Codemaster Freders: "I hope you're going to enjoy the browser experience with Ubuntu Touch 20.04. Camera patched back up as well as preliminary support for VP8 & VP9 HW video decoding. 😊"](https://mastodon.social/@fredldotme/109580335078462509)
_There's been an interesting newsletter, but it hasn't been posted to the blog yet. If you're not subscribed, you can obtain a copy of it [here](https://linmob.uber.space/ubports-newsletter-2022-12-30.html)._

#### Sailfish OS
- [Sailfish Community News, 29th December, Happy New Year!](https://forum.sailfishos.org/t/sailfish-community-news-29th-december-happy-new-year/13992)

#### Distributions
- Manjaro Blog: [December 2022 in Manjaro ARM](https://blog.manjaro.org/december-2022-in-manjaro-arm/)

#### Stack
- Phoronix: [Mesa 22.3.2 Closes Out The Year With RADV RT Fixes, Raspberry Pi V3DV Fixes Too](https://www.phoronix.com/news/Mesa-22.3.2-Released)
- Phoronix: [Mesa Open-Source 3D Drivers Experienced Record Growth In 2022](https://www.phoronix.com/news/Mesa-3D-Driver-Growth-2022)

#### Linux
- Phoronix: [New Patches Aim To Reduce Memory Use While Compiling The Linux Kernel](https://www.phoronix.com/news/Linux-Kernel-Build-Less-RAM)
- Phoronix: [Linux 6.1.2 Closed Out 2022 With Many Backported Fixes](https://www.phoronix.com/news/Linux-6.1.2-Released)

#### Non-Linux
- Lee Lup Yuen: [NuttX RTOS for PinePhone: LCD Panel](https://lupyuen.github.io/articles/lcd)
- Lee Lup Yuen: [NuttX RTOS for PinePhone: Framebuffer](https://lupyuen.github.io/articles/fb)

#### Matrix
- Neko.dev: [Matrix Community Year In Review 2022 - blog.neko.dev](https://blog.neko.dev/posts/matrix-year-in-review-2022.bhtml)

### Worth noting
- Lemmy - linuxphones: [I've added a pr for Lemmur flatpak support](https://lemmy.ml/post/677871)
- Lemmy - linuxphones: [Waydroid helper app - install / uninstall on Ubuntu Touch](https://lemmy.ml/post/678942)
- [Adam Dawson: "Turns out the #Librem5 does have automatic camera controls.  I just installed from source and its worth the trouble. Works great!"](https://social.sdf.org/@adamd/109611034092328091). _You can also [just download debs Purism's CI/CD pipeline](https://source.puri.sm/Librem5/millipixels/-/pipelines)._
- LINux on MOBile: ["Going retro: This is the Motorola A910, my first Linux phone that supported WiFi."](https://fosstodon.org/@linmob/109603099077003702) _Fun fact: With out that little device, this blog would have never been started!_


### Worth reading
- Rayyan Ansari: [Mainlining Nexus 9: First Boot](https://rayyanansari.github.io/posts/mainline-nexus-9-p1/). _Mainlining a device is so much work!_
- chfkch: [NixOS on the Pinephone Pro](https://chfkch.codeberg.page/nixos-pinephonepro/#org6c21cc6). _Nice!_
- Lee Lup Yuen: [NuttX RTOS for PinePhone: What is it?](https://lupyuen.github.io/articles/what) _In case you were wondering._
- Nate Graham: [Highlights from 2022](https://pointieststick.com/2022/12/31/highlights-from-2022/)
- Guido Günther: [Phosh 2022 in retrospect](https://honk.sigxcpu.org/con/Phosh_2022_in_retrospect.html). _Thanks for your work!_
- LINux on MOBile: [Looking back, looking forward (2022/2023)](https://linmob.net/looking-back-looking-forward/)
- AksDev: [How to use Matrix](https://www.akselmo.dev/2022/12/29/How-To-Use-Matrix.html) _In case you want to start using Matrix or secure your digital life in 2023, this one and the one below should be helpful._
- gruenich: [KDE policy made my digital life more secure](https://gruenich.blogspot.com/2022/12/kde-policy-made-my-digital-life-more-secure.html)
- Aaron's Blog: [Mobile Linux (Phosh): The issues with Linux on Mobile devices](https://ahoneybun.github.io/blog/mobile-linux-phosh/)[^1]
- Aaron's Blog: [Mobile NixOS: Running NixOS on mobile devices like the PinePhone](https://ahoneybun.github.io/blog/mobile-nixos/) [^1]

### Worth watching
- TechHut: [I used a Linux phone for 30 days](https://www.youtube.com/watch?v=qmlabKbpTGM)
- Jozef Mlich: [USB mode selection on Nemo](https://www.youtube.com/watch?v=XaICulr68T4)
- 𝕷𝖊𝖌𝖔𝖘𝖎 𝕴𝖓𝖚𝖌𝖆𝖒𝖎 🐺: [Remaster Maemo Leste Chimaera using Systemback.](https://www.youtube.com/watch?v=0kp-X94ouA0)
- Sabellus: [Entering Russian characters in maemo leste Nokia N900](https://www.youtube.com/watch?v=mNo6FnZNDuA)

### Thanks

Huge thanks again to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one! __If you just stumble on a thing, please put it in there too - all help is appreciated!__

PS: I'm looking for feedback - [what do you think?](mailto:weekly-update@linmob.net?subject=Feedback%20on%20Weekly%20Update)

[^1]: This article was added after initial publication around 4 pm UTC on January 2nd, 2023.
