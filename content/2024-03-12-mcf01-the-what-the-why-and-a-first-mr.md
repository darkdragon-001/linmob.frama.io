+++
title = "MCF01: The what, the why and a first MR"
date = "2024-03-12T18:53:19Z"
draft = false
[taxonomies]
tags = ["MobileLinux", "mobile-config-firefox", "Firefox",]
categories = ["projects"]
authors = ["Peter"]
+++

This is the first post in an attempt to start a series of blog posts about how one works through making something work, inspired by flypig and piggz.

It's about a project [I contributed to earlier](https://fosstodon.org/@linmob/106811013846424578), [mobile-config-firefox](https://gitlab.com/postmarketOS/mobile-config-firefox).
<!-- more -->

I believe mobile-config-firefox is a really important project for #MobileLinux, and am I at least close to the skill-level necessary to make improvements to it. Also, there's no need to re-invent the wheel, as there's plenty in [user0's branch](https://codeberg.org/user0/mobile-config-firefox/src/branch/fenix) that deserves to land in distributions. The goal of this endeavour is to bring improvements to mobile-config-firefox, that make it so that Firefox ESR 128 is well supported when Mozilla [releases it on July 9th, 2024](https://whattrainisitnow.com/calendar/).

All this project takes is taking the time to experiment, test, and keeping at it until one makes it work. In fact, I already started with a [tiny MR to fix an issue](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/41) - please take the time to read the MR and the [issue](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/66).

The process to come up with this MR was iterative. First, [I saw and read the issue](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/66), obviously. I then figured out how to hide the translation button in the URL bar, which was super easy - it showed up in the window I was debugging. I then considered it smart to also hide the picture-in-picture button in the URL (if you disagree, chime in on the [MR](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/41), btw). This was trial-and-error guesswork, that worked at first try - fortunately Mozilla used a sane and obvious CSS ID here.

And that's it for today - come back next time, it will likely be something else from the [issue tracker](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues).
