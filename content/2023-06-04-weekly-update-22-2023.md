+++
title = "Weekly GNU-like Mobile Linux Update (22/2023): Phosh 0.28.0 and an U-boot boot menu for the PinePhone Pro"
date = "2023-06-04T14:20:11Z"
draft = false
[taxonomies]
tags = ["Sailfish OS", "Ubuntu Touch", "Phosh", "postmarketOS", "PinePhone Pro", "u-boot",]
categories = ["weekly update"]
authors = ["Peter",]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Megi has been on fire, Manjaro release another Phosh beta, a Sailfish Community Update and more!
<!-- more -->
_Commentary in italics._

### Hardware
- [NEOklacker | Hackaday.io](https://hackaday.io/project/190358-neoklacker) _Yet another handheld, but this one looks quite appealing to everybody into the BlackBerry form factor!_

### Software

#### Gnome Ecosystem
- This Week in GNOME: [#98 Fast Searching](https://thisweek.gnome.org/posts/2023/06/twig-98/)
- ebassi: [Configuring portals](https://www.bassi.io/articles/2023/05/29/configuring-portals/)
- Phosh.mobi: [Phosh 0.28.0](https://phosh.mobi/releases/rel-0.28.0/) _Great release, fixing all issues with the new power-menu introduced in 0.27.0 and more!_

#### Plasma Ecosystem
- Nate Graham: [This week in KDE: for developers](https://pointieststick.com/2023/06/02/this-week-in-kde-for-developers/)
- redstrate: [My work in KDE for May 2023](https://redstrate.com/blog/2023/05/my-work-in-kde-for-may-2023/)

#### Sailfish OS
- [Sailfish Community News, 1st June, Summer Greetings](https://forum.sailfishos.org/t/sailfish-community-news-1st-june-summer-greetings/15784)

#### Ubuntu Touch
- @OyasumiBastet: [Just a reminder for those in the Linux Phone community.

There are lots of things the UBport's community and development team would love to have;
- Newer QT
- Snaps/Flatpaks
- More versatile background processes
And lots more

The only way we can get there, is with more help!](https://nitter.net/OyasumiBastet/status/1663246297648750593#m)

#### Distributions
- Mobian Wiki: [desktopenvironments - Added pictures for lomiri and some installation instructions and replaced the plasma mobile photos](https://wiki.mobian-project.org/doku.php?id=desktopenvironments&rev=1685834769&do=diff) _Documentation for Lomiri on Mobian, yay!_
- Manjaro PinePhone Phosh: [Beta 32](https://github.com/manjaro-pinephone/phosh/releases/tag/beta32)

#### Kernel
- Phoronix: [Qualcomm Adreno 600 Series Graphics Get OpenGL 4.6 On Open-Source Driver](https://www.phoronix.com/news/Qualcom-A600-OpenGL-4.6) _If you're wondering: Does this benefit my SoC? Snapdragon 845 has Adreno 630 graphics, so plenty great Linux-running smartphones are going to benefit from this._
- @calebccff@fosstodon.org: [Thanks to nt8r for getting this fixed, the SDM845 devices have such long kernel cmdline that it literally broke dmesg, we love #Android bootloader...https://github.com/util-linux/util-linux/pull/2279#LinuxMobile #SDM845 #postmarketOS #kernel](https://fosstodon.org/@calebccff/110464972513875676)

#### Non-Linux
- Lup Yuen: [LVGL for PinePhone (and WebAssembly) with Zig and Apache NuttX RTOS](https://github.com/lupyuen/pinephone-lvgl-zig#feature-phone-ui)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-06-02](https://matrix.org/blog/2023/06/02/this-week-in-matrix-2023-06-02)

### Worth Noting
- @linmob@fosstodon.org: [PSA by @danctnix: https://nitter.net/DanctNIX/status/1662199534808883200#m](https://fosstodon.org/@linmob/110438792776308748)
- PINE64official (Reddit): [PineTab2s Are Shipping!!!](https://www.reddit.com/r/PINE64official/comments/13y3nvy/pinetab2s_are_shipping/)
- Phones (Librem 5),- Purism community: [Tutorial: Add a custom sound to Gnome-Clocks](https://forums.puri.sm/t/tutorial-add-a-custom-sound-to-gnome-clocks/20434)
- @dos@librem.one: [Experimenting with utilizing assisted GNSS techniques on #Librem5. Cold fix takes about 3 minutes in perfect conditions, but can easily take *much* longer otherwise - that&apos;s how GPS works. However, by downloading satellite data from the Internet instead of the sky it can go down to under a minute; sometimes even just a few seconds. Still a proof-of-concept at this point, but can already tell that catching a fix gets significantly easier this way:) #mobile #gnu #linux #gnss #gps #galileo #glonass](https://social.librem.one/@dos/110442760235221039)

### Worth Reading
- Hamblingreens Blog: [Pinephone Screen Replacement](https://hamblingreen.com/pinephone-screen-replacement)
- Purism: [Librem 5 Battery Life Improved by 100%!](https://puri.sm/posts/librem-5-battery-life-improved-by-100/) _Looks like I need to put my SIM out of the Xperia 10 III and back into the Librem 5 to test this!_
- fossphones.com: [Linux Phone News - June 1, 2023](https://fossphones.com/06-01-23.html) _Nice round-up!_
- Jorge Castro: [The distribution model is changing](https://www.ypsidanger.com/the-distribution-model-is-changing/)
- Martijn Braam: [Developers are lazy, thus Flatpak](https://blog.brixit.nl/developers-are-lazy-thus-flatpak/) 
  - a response: [Sonny: "Instead of complaining about a…" - FLOSS.social](https://floss.social/@sonny/110482367773271228) _Weirdly enough, I think that the authors of the three links above are fighting for the same side. Let's not turn this into a full blown fight about details, that then only makes Apple, Microsoft or Google happy (if they manage to care enough). Yes, there are issues (I would love for flatpak automagically removing unused runtimes and GNOME Software ain't perfect), but they can all be fixed!_
- Megi: [2023–05–29: Touch screen and touch menu support for Pinephone Pro in U-Boot](http://xnux.eu/log/086.html)
- Megi: [2023–05–28: Some regressions in power use in system sleep on original Pinephone](http://xnux.eu/log/085.html)
- Megi: [2023–05–27: Implementing display support in U-Boot for Pinephone Pro](http://xnux.eu/log/084.html)
- Megi: [2023–05–25: DRAM frequency scaling on Pinephone Pro saves 0.5W of power!](http://xnux.eu/log/083.html)
- Megi: [2023–05–23: Boot time power consumption tracing](http://xnux.eu/log/082.html)
- Megi: [2023–05–22: Pinephone (Pro) power measurements and optimizations](http://xnux.eu/log/081.html)

### Worth Listening
- postmarketOS Podcast: [#31 Testing Team, KDE6, SDM845 Sensors, ALIT, PineNote, L10N](https://cast.postmarketos.org/episode/31-Testing-Team-KDE6-SDM845-Sensors-ALIT-PineNote-L10N/) _Great episode!_

### Worth Watching
- sadfwesv: [Pinephone Pro U-Boot with some improvements](https://www.youtube.com/watch?v=j21dkwYfYg0)
- devrtz: [Running Debian on a Smartphone](https://meetings-archive.debian.net/pub/debian-meetings/2023/Debian-Reunion-Hamburg/debian-reunion-hamburg-3-running-debian-on-a-smartphone.webm)
- Vega Data: [Explorando o Plasma Mobile no postmarketOS ](https://www.youtube.com/watch?v=E4bAHZXu4e0)
- NieRox NRX: [Linux postmarketos plasma desktop on Xiaomi redmi2 ](https://www.youtube.com/watch?v=CNubw2TOrK4)
- Prend Workbench: [How to Build and Install PostmarketOS Linux Yourself on Supported Phones ](https://www.youtube.com/watch?v=TSm_nbvuDFU)
- Lup Yuen: [Testing the #NuttX Feature Phone UI for #PinePhone ... With #LVGL, #ZigLang and WebAssembly ](https://www.youtube.com/shorts/iKa0bcSa22U)
- Unboxing Tomorrow: [PinePhone Beta: I2C Breakout Unboxing ](https://www.youtube.com/watch?v=vyOKL58jyX4)
- qkall: [Pinephone Pro - Arch Testing - SXMO - May 2023 ](https://www.youtube.com/watch?v=OMTInocUReE)
- Continuum Gaming: [Continuum Gaming E367: Sailfish OS – How to activate the 3 rear cameras on the Sony Xperia 10 III ](https://www.youtube.com/watch?v=cGQ44Vrzao8)

### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

