+++
title = "Weekly GNU-like Mobile Linux Update (29/2023): WebRTC with video in Capyloon and emergency calls in Phosh"
date = "2023-07-23T20:45:41Z"
draft = false
[taxonomies]
tags = ["Ubuntu Touch", "Capyloon", "Droidian", "Panfrost", "PinePhone Pro", ]
categories = ["weekly update"]
authors = ["Peter",]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Also: A 2nd gen Juno Tablet, a new release of Tuba, Arm embracing Panfrost development, a new release of unofficial Ubuntu Touch for PINE64 devices, Widewine support for Ubuntu Touch 20.04  and more!

<!-- more -->
_Commentary in italics._

### Events
- Guido Günther: [There will be a #LinuxMobile #devroom at #FroSCon2023:https://froscon.org/programm/projektraeume/It&apos;s open both days for #hacking, discussion and ad-hoc sessions but you can also schedule a talk:https://mensuel.framapad.org/p/froscon23-mobileHopefully see you there!](https://social.librem.one/@agx/110751696818636694) _[FroSCon takes place 2023-08-05 and 2023-08-06 in St. Augustin near Bonn in Germany](https://froscon.org/info/]._

### Hardware
- Liliputing: [Juno Tab 2 Linux tablet packs a bigger screen, more storage (and the same Jasper Lake processor)](https://liliputing.com/juno-tab-2-linux-tablet-packs-a-bigger-screen-more-storage-and-the-same-jasper-lake-processor/)

### Software

#### Gnome Ecosystem
- This Week in GNOME: [#105 Legendary Saturday Edition](https://thisweek.gnome.org/posts/2023/07/twig-105/)
- [Alice: "libadwaita update, long Last …"](https://crab.garden/@alice/110750497219400506)
- theevilskeleton: [Opt-in Telemetry and Asking Users for Feedback May Not Work in Practice](https://theevilskeleton.gitlab.io/2023/07/16/opt-in-telemetry-and-asking-users-for-feedback-may-not-work-in-practice.html)

#### Plasma Ecosystem
- Nate Graham: [This week in KDE: Plasma 6 features](https://pointieststick.com/2023/07/21/this-week-in-kde-plasma-6-features/)
- Nate Graham: [Where bugfixes and new features come from](https://pointieststick.com/2023/07/16/where-bugfixes-and-new-features-come-from/)
- tsdgeos: [KDE Gear 23.08 branches created](https://tsdgeos.blogspot.com/2023/07/kde-gear-2308-branches-created.html)
- [Camilo: "mauikit, mauiman, maui shell and more projects have already been ported to Qt6.…"](https://fosstodon.org/@camiloh/110729827689410586)

#### Ubuntu Touch
- Unofficial Ubuntu Touch for PinePhone (Pro): [v0.8](https://gitlab.com/ook37/pinephone-pro-debos/-/releases/v0.8)
- fredldotme: [Uh oh, what could that toot be about??Snaps using libhybris drivers on Ubuntu Touch of course!](https://mastodon.social/@fredldotme/110734066879770280)
- [widenabler - OpenStore](https://open-store.io/app/widenabler.mariogrip) _Widewine DRM support, but... be careful!_

#### Capyloon
- Capyloon: [Fri 09 June 2023](https://capyloon.org/releases.html#jul-21-2023)
- [Capyloon: "With a couple of patches from …"](https://fosstodon.org/@capyloon/110731752382326647) _WebRTC with camera support on PPP!_

#### Matrix
- Matrix.org: [This Week in Matrix 2023-07-21](https://matrix.org/blog/2023/07/21/this-week-in-matrix-2023-07-21/)
- Matrix.org: [A giant leap forwards for encryption with MLS](https://matrix.org/blog/2023/07/a-giant-leap-with-mls/)

#### Distributions
- Breaking updates in pmOS edge: [/etc/boot/cmdline.txt is no longer used to configure kernel parameters on boot](https://postmarketos.org/edge/2023/07/20/boot-deploy-cmdline/)
- Manjaro PinePhone Phosh: [Beta 34](https://github.com/manjaro-pinephone/phosh/releases/tag/beta34)

#### Linux
- CNX Software: [Linux 6.4 release - Main changes, Arm, RISC-V and MIPS architectures](https://www.cnx-software.com/2023/06/26/linux-6-4-release-main-changes-arm-risc-v-and-mips-architectures/)

#### Stack
- Collabora: [A helping Arm for Panfrost](https://www.collabora.com/news-and-blog/news-and-events/a-helping-arm-for-panfrost.html)
  - Phoronix: [Arm Talks Up Their Open-Source Contributions, Adding Support For Panfrost](https://www.phoronix.com/news/Arm-Talks-Up-Open-Source)
- Phoronix: [XWayland 23.2 RC1 Brings Tearing Control, Resizable Rootful, Emulated Input](https://www.phoronix.com/news/XWayland-23.2-RC1)

### Worth Noting
- [GeopJr: "#Tuba v0.4.0 is now available,…"](https://tech.lgbt/@GeopJr/110738491040913437)
- [Martijn Braam: "This took a lot of work "](https://fosstodon.org/@martijnbraam/110759237105159767). _Megapixels progress!_
- [Seth Falco: "Finally reading #PGP encrypted email on my #PinePhonePro. 🎉 With the latest version of #postmarketOS and #Thunderbird, it's still shoddy, but we can actually set up and read emails. No issues with launching or window sizing!…"](https://fosstodon.org/@sethi/110754272508110433)
- [𖤐 MadameMalady 𖤐: "I made an update to My Waydroid Manager…"](https://strangeobject.space/@FOSSingularity/110737504897547159)
- [Chris Gioran: "Mocking Google for their totally-not-an-attempt-to-DRM-the-Web is the right thing to do.…"](https://fosstodon.org/@chrisg/110752272862167301)
- PinePhone (Reddit): [What OS just works?](https://www.reddit.com/r/pinephone/comments/155so0j/what_os_just_works/)
- PINE64official (Reddit): [[Development] Baremetal/low-level kernel development for Pinephone pro](https://www.reddit.com/r/PINE64official/comments/156y5wi/development_baremetallowlevel_kernel_development/)
- PINE64official (Reddit): [Pine Phone Pro usability?](https://www.reddit.com/r/PINE64official/comments/155cptm/pine_phone_pro_usability/)
- Phones (Librem 5),- Purism community: [Selcting items in context menues - faulty behaviour?](https://forums.puri.sm/t/selcting-items-in-context-menues-faulty-behaviour/20871)

### Worth Reading
- Camden Bruce: [The ultimate Linux phone guide for consumers](https://medium.com/@camden.o.b/the-ultimate-linux-phone-guide-28d0cce9929)
- Phosh.mobi blog: [How Phosh and GNOME Calls handle emergency calls](https://phosh.mobi/posts/emergency-calls/)
- LINux on MOBile: [State of the Droidian Project](https://linmob.net/droidian-project/) _Thanks again for the guest post, FakeShell!_
- Purism: [Free Yourself from Surveillance Capitalism with the Liberty Phone](https://puri.sm/posts/free-yourself-from-surveillance-capitalism-with-the-liberty-phone/) 

### Worth Listening
- Linux After Dark: [Episode 48 – What ever happened to convergence?](https://linuxafterdark.net/linux-after-dark-episode-48/)

### Worth Watching
- La Chaine en Vrac de Joel: [TUTO How to Install Kali Linux NetHunter Pro on Pinephone Pro Without Computer [ENG] - Phosh UI](https://www.youtube.com/watch?v=1NsHLUu9IkU)
- Linux Mobile: [Juno Tab 2 - Intro (Linux Tablet)](https://www.youtube.com/watch?v=VEd1953OMx4)
- IT Mobile: [Google Nexus 5 Ubuntu Touch Incoming call](https://www.youtube.com/watch?v=AFKGTUZiH-Y)
- kajuz6: [Sailfish OS 4.5.0.21 quick video changelog! Mid summer update for all Jolla Devices!](https://www.youtube.com/watch?v=H9efvd9PJz8)
- NOT A FBI Honey pot: [Pine Phone's most beautiful Linux phone Distro #shorts :RhinoOS](https://www.youtube.com/watch?v=hW3GulDaXA4)
- PC FREEDOM: [【スマホで動くLinux】Droidian：もうスマホのOSはAndroidだけじゃない #pixel3a](https://www.youtube.com/watch?v=_b8lwrF1Kos)

### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

