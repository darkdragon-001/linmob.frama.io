+++
title = "Weekly GNU-like Mobile Linux Update (51/2023): postmarketOS 23.12 and Happy Holidays!"
date = "2023-12-24T17:40:08Z"
draft = false
[taxonomies]
tags = ["Sailfish OS", "Ubuntu Touch", "FOSDEM 2024", "37C3", "postmarketOS", "Phosh", "Manjaro",]
categories = ["weekly update"]
authors = ["Peter",]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Hohoho, time for one of the lasts Weekly Updates of 2023[^1], bringing many nice things: postmarketOS 23.12, new Manjaro images for PP/PPP, a Phosh bugfix release, an explainer how to switch On Screen Keyboards in Phosh, and many more free software gifts!

<!-- more -->
_Commentary in italics._

### Hardware
- Liliputing: [ZeroWriter: Open source, DIY E Ink typewriter that costs about $200 (or less) to build](https://liliputing.com/zerowriter-open-source-diy-e-ink-typewriter-that-costs-about-200-or-less-to-build/)
- [joey castillo: "I'm ever so stoked to share that I'll be giving a talk at #fosdem 2024! The topic is “Comprehensible Open Hardware,” and I'm looking forward to sharing some lessons learned designing the Open Book and other open hardware projects.…"](https://mastodon.social/@joeycastillo/111608084182723876)

### Software

#### Gnome Ecosystem
- This Week in GNOME: [#127 Welcome News](https://thisweek.gnome.org/posts/2023/12/twig-127/)
- Marcus Lundblad: [Christmas Maps 2023](https://ml4711.blogspot.com/2023/12/christmas-maps-2023.html)
- wjjt: [Removing Online Accounts from GNOME Initial Setup 46](https://blogs.gnome.org/wjjt/2023/12/20/removing-online-accounts-from-gnome-initial-setup-46/)
- Planet GNOME: [Making SSH host certificates more usable](https://mjg59.dreamwidth.org/68721.html)
- Planet GNOME: [#2 Another update on GNOME Settings](https://feborg.es/settings-news-2/)

#### Phosh
- Phosh.mobi blog: [Phosh's On Screen Keyboard interface](https://phosh.mobi/posts/phosh-osk-interface/)
- Phosh.mobi: [Phosh 0.34.1](https://phosh.mobi/releases/rel-0.34.1/)

#### Plasma Ecosystem
- Nate Graham: [This week in KDE: Holiday bug fixes](https://pointieststick.com/2023/12/22/this-week-in-kde-holiday-bug-fixes/)
- Volker Krause: [KDE @ 37C3](https://www.volkerkrause.eu/2023/12/23/kde-37c3.html)
- KDE Announcements: [KDE's 6th Megarelease - Beta 2](https://kde.org/announcements/megarelease/6/beta2/)
- Planet KDE: [Qt Contributor’s Summit 2023](https://blog.broulik.de/2023/12/qt-contributors-summit-2023/)
- Carl Schwan: [Announcing Brise theme](https://carlschwan.eu/2023/12/19/announcing-brise-theme/)
- jriddell: [XWayland Video Bridge 0.4](https://jriddell.org/2023/12/18/xwayland-video-bridge-0-4/)
- zamundaaa: [An update on HDR and color management in KWin](https://zamundaaa.github.io/wayland/2023/12/18/update-on-hdr-and-colormanagement-in-plasma.html)
- Phoronix: [KDE Developers Prepare For Christmas With More Bug Fixes & Qt 6 Porting](https://www.phoronix.com/news/KDE-Bug-Fixes-For-Christmas)

#### Sailfish OS
- flypig's Gecko log: [Day 116](https://www.flypig.co.uk/list?list_id=963&list=gecko)
- flypig's Gecko log: [Day 115](https://www.flypig.co.uk/list?list_id=962&list=gecko)
- flypig's Gecko log: [Day 114](https://www.flypig.co.uk/list?list_id=960&list=gecko)
- flypig's Gecko log: [Day 113](https://www.flypig.co.uk/list?list_id=959&list=gecko)
- flypig's Gecko log: [Day 112](https://www.flypig.co.uk/list?list_id=958&list=gecko)

#### Ubuntu Touch
- UBports News: [Ubuntu Touch Q&A 126](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-q-a-126-3908)
- Ubuntu Touch Forums News: [Q&amp;A 130 Blog and Audio now available](https://forums.ubports.com/topic/9681/q-a-130-blog-and-audio-now-available)
- fredldotme: [Pocket VMs running GL-accelerated Fedora on Ubuntu Touch on the PineTab 2.Nobody asked for this, yet here we are.](https://mastodon.social/@fredldotme/111625704562612216)
- fredldotme: [PineTab 2 is in the process of getting the immutable filesystem treatment on Ubuntu Touch. I'm really starting to like this device!](https://mastodon.social/@fredldotme/111613820088420401)

#### Distributions
- postmarketOS Blog: [v23.12: The One We Asked The Community To Name](https://postmarketos.org/blog/2023/12/18/v23.12-release/)
- postmarketOS pmaports issues: [Create data migration project](https://gitlab.com/postmarketOS/pmaports/-/issues/2457)
- postmarketOS Wiki New Pages: [Sxmo/How incoming calls work](https://wiki.postmarketos.org/wiki/Sxmo/How_incoming_calls_work)
- postmarketOS Wiki New Pages: [Backing up a non-pmOS phone](https://wiki.postmarketos.org/wiki/Backing_up_a_non-pmOS_phone)
- Manjaro PinePhone Plasma Mobile: [Beta 15](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/beta15)
- Manjaro PinePhone Phosh: [Beta 36](https://github.com/manjaro-pinephone/phosh/releases/tag/beta36)

#### Kernel
- phone-devel: [[PATCH 0/3] Fairphone 5 PMIC-GLINK support (USB-C, charger, fuel gauge)](http://lore.kernel.org/phone-devel/20231220-fp5-pmic-glink-v1-0-2a1f8e3c661c@fairphone.com/)

#### Stack
- Phoronix: [Wayland-Proxy Load Balancer Helping Firefox Cope With Wayland Issues](https://www.phoronix.com/news/Wayland-Proxy-Firefox)
- Phoronix: [Enlightenment 0.26 Released With Various Improvements](https://www.phoronix.com/news/Enlightenment-0.26-Released)
- Phoronix: [Qt 6.7 Beta Released With Variable Font Support & QRhiWidget Class](https://www.phoronix.com/news/Qt-6.7-Beta)

#### Matrix
- Matrix.org: [The Governing Board, our next big step in open governance](https://matrix.org/blog/2023/12/electing-our-first-governing-board/)


### Worth Noting
- Jozef Mlich on Fosstodon: [Schedule of FOSS on Mobile Devices devroom is online.Now I can see it that it will be at Saturday, not Sunday. #FOSDEMhttps://fosdem.org/2024/schedule/track/foss-on-mobile-devices/](https://fosstodon.org/@jmlich/111605991428775975)
- [Krille-chan: "🔔 FluffyChat needs tester 🔔…"](https://mastodon.art/@krille/111623928217019311)
- [Tuba: "#Tuba v0.6.0 is now available,…"](https://floss.social/@Tuba/111625413323146907)
- [as400: "Having my OP6 back in a workin…"](https://mas.to/@as400/111579505807216263)
- [David Heidelberg: "#Minecraft #minetest #mobileLi…"](https://floss.social/@okias/111618384689851551)
- [minute: "i accidentally made a linux phone (the key was bitclock-inversion)…"](https://mastodon.social/@mntmn/111620052555788734)
- [Jan Vlug: "#FOSS on #Mobile Devices track…"](https://mastodon.social/@janvlug/111611750780659114)
- [Adam Honse: "Just had a thought...the OnePlus 6/6T have a physical switch used for silent/vibrate/ring in Android that is unused on mobile Linux.…"](https://mastodon.social/@CalcProgrammer1/111613314552048876)
- [Project Insanity: "Added a small section to the #NixOS wiki on how to configure "dark mode" for #QT applications with #GNOME integration …"](https://social.project-insanity.org/@pi_crew/111635237141224193)
- [justsoup: "Some more context on the #Lomiri #postmarketOS porting efforts 1/?:…"](https://mstdn.social/@justsoup/111632873472470989)
- [Sailfish OS News Network: "Announcing Chum Web The Chum community created a website which is useful for browsing all the available apps without using the #Chum GUI 'App Store' on #SailfishOS.…"](https://mastodon.social/@sailfishosnews/111631176855185137)
- Purism community: [Crimson Experience](https://forums.puri.sm/t/crimson-experience/22228)
- Purism community: [Last update phone stopped working](https://forums.puri.sm/t/last-update-phone-stopped-working/22205)
- @dos@librem.one: [So turns out I&apos;m going to be speaking at #FOSDEM in February as part of the &quot;FOSS on Mobile Devices&quot; track. We&apos;ll go step-by-step through debugging a spontaneous modem reset issue that used to trouble the #Librem5 phone, which - spoiler alert - turned out to be a (not very well-)known bug in the #USB 2.0 spec. Check this and other talks out at https://fosdem.org/2024/schedule/track/foss-on-mobile-devices #fosdem24 #fosdem2024 #linuxmobile](https://social.librem.one/@dos/111607735451156582)

### Worth Reading
- Jonas’ blog: [A dive into Jolla AppSupport](https://blogs.gnome.org/jdressler/2023/12/20/a-dive-into-jolla-appsupport/)
- Martijn Braam: [Megapixels 2.0: DNG loading and Autowhitebalance](https://blog.brixit.nl/megapixels-2-0-dng-loading-and-whitebalancing/)
- eighty-twenty news: [On the harm caused by missing basic (basic!) functionality in Signal, WhatsApp, Android and iOS](https://eighty-twenty.org/2023/12/17/transferring-android-to-iphone)
- Purism: [Opportunistic Word Completion](https://puri.sm/posts/opportunistic-word-completion/)
- Phoronix: [Linux 6.8 To Add Support For Several Cheap ARM-Powered Handheld Game Consoles](https://www.phoronix.com/news/Linux-6.8-Cheap-Gam-Consoles)
- Phoronix: [Acer Aspire 1 ARM Laptop Has Nearly Complete Upstream Linux Support](https://www.phoronix.com/news/Acer-Aspire-1-EC-Linux-Complete)

### Worth Watching
- [The Story Of Festivus | Seinfeld](https://www.youtube.com/watch?v=1njzgXSzA-A). _First things first!_
- pinephone pro user: [@stockdroid - setup guide for the pinephone pro to work right, revised](https://www.youtube.com/watch?v=XZkCvwMxH_4)
- CodingBite: [PostmarketOS 23.12 is Here: Breathing New Life into Smartphones!](https://www.youtube.com/watch?v=OTGq_6B8kUw)
- Tong Zou: [ZTE Open C (Firefox OS), Samsung Z1 (Tizen OS) retrospective](https://www.youtube.com/watch?v=rRFUX2wcENY). _I am pretty sure this and the following are re-uploads._
- Tong Zou: [PinePhone (Manjaro Linux) unboxing + overview - Nice phone for hackers?](https://www.youtube.com/watch?v=v3nUaE0rzUc)


### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__


[^1]: This may well be the last Weekly Update published for 2023. The next one is tentatively scheduled for December 30th, 2023 - but I am not sure if I will be able to deliver it after having gone/partied/survived through [37C3](https://events.ccc.de/congress/2023/infos/index.html).
