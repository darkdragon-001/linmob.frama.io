+++
title = "MCF02: First shave some Yaks"
date = "2024-03-13T20:11:15Z"
draft = false
[taxonomies]
tags = ["MobileLinux", "mobile-config-firefox", "Firefox"]
categories = ["projects"]
authors = ["Peter"]
+++

First: It's amazing, [my MR](https://gitlab.com/postmarketOS/mobile-config-firefox/-/merge_requests/41) (well, [plural](https://codeberg.org/user0/mobile-config-firefox/pulls/2)) got merged already! _Thanks user0 and ollieparanoid!_
I figured it might be important to get to a sane setup early on.

<!-- more -->

After all, as I don't work with code regularily, I am not very clever with this. The following may still be bad advice, I just read [a few forum thread](https://forum.gitlab.com/t/refreshing-a-fork/32469/) (I'll happily incorporate future feedback), but I figured I should first set up my local git repo properly.

I figured that I, as I also try to get things I fix into user0's fork, should set up one working repo of my own with multiple remotes.
- origin is my codeberg repo, as codebergs web interface is less annoying on slower devices - I'll push all my attempts there,
- gitlab is my gitlab repo, where I'll push my working branches that target upstream,
- upstream is postmarketOS/mobile-config-firefox
- and user0 is user0's mobile-config-firefox repo, the upstream of the fenix branch.

This way, I have it all in one. Before I start a feature branch, I will have to remember to pull `master` and `fenix` from their respective upstreams - and do work in separate branches, if an issue affects both. I then should update my two repos.

Also, testing. I figure I should test my changes on
- current ESR to avoid regressions,
- current release,
- future release (beta, dev, nightly) - one of those, depending on availability for aarch64.

The 'future release' may be hard, as it could be challenging to obtain builds for aarch64. I am not sure what's available [beyond the Snap](https://snapcraft.io/firefox), assuming it actually works - I've had two "arm64 Snaps" that then would not run, due to being actually amd64/x86_64 so far in my "app testing" endeavours.

And, I think providing before/after screenshots with larger MRs may make sense.

Aside from the yak shaving, the only contribution done since the [last post](https://linmob.net/mcf01-the-what-the-why-and-a-first-mr/) was [this comment](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/64#note_1812084509) on an open issue.

See you soon!
