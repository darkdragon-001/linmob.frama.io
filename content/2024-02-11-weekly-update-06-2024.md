+++
title = "Weekly GNU-like Mobile Linux Update (06/2024): More FOSDEM talks and releases of pmbootstrap and Megapixels"
date = "2024-02-11T14:24:36Z"
draft = false 
[taxonomies]
tags = ["Sailfish OS", "Ubuntu Touch", "Phosh", "Sxmo", "Megapixels", "pmbootstrap",]
categories = ["weekly update"]
authors = ["Peter",]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

A Sailfish OS Community News update, a postmarketOS report, Plasma 6 coming closer, a bunch of Sxmo userscripts and finally an overdue quaterly update by LinuxPhoneApps.org.

<!-- more -->
_Commentary in italics._

### Hardware
- Liliputing: [Fairphone 5 schematics, repair, and recycling documentation is now available](https://liliputing.com/fairphone-5-schematics-repair-and-recycling-documentation-is-now-available/)

### Software

#### Gnome Ecosystem
- This Week in GNOME: [#134 High Contrast](https://thisweek.gnome.org/posts/2024/02/twig-134/)
- James Westmans blog: [New Look for GNOME Maps!](https://www.jwestman.net/2024/02/10/new-look-for-gnome-maps.html)

#### Phosh
- @arunmani: [Today, after nearly half a year of development, we have landed the ability to select Wi-Fi networks from the top-panel. The best part about this feature is, now that we have decided over the structure, we can implement the same feature for Bluetooth, VPNs, etc. easily.A big thanks to @agx for his feedback, patches and review throughout the process!#phosh #librem5 #gnome #linux #mobile #LinuxMobile #MobileLinux](https://mastodon.social/@arunmani/111879980566330621)
- #phosh: [Why am I urged to make the #Phosh ticket box more plugin based ? I mean, one could have a DBus API for drop-in ticket providers so that we could e.g. use #KDEItinerary or #fWallet as external ticket providers in there 👀 ?https://gitlab.gnome.org/World/Phosh/phosh/-/tree/main/plugins/ticket-box?ref_type=heads](https://alpaka.social/@braid/111889036578956372)

#### Plasma Ecosystem
- Nate Graham: [This week in KDE: Inching closer](https://pointieststick.com/2024/02/09/this-week-in-kde-inching-closer/)
- Nate Graham: [15-Minute Bug Initiative update](https://pointieststick.com/2024/02/08/15-minute-bug-initiative-update/)
- KDE Announcements: [KDE Ships Frameworks 5.115.0](https://kde.org/announcements/frameworks/5/5.115.0/)
- Broulik: [Qt Wayland, Supercharged](https://blog.broulik.de/2024/02/qt-wayland-supercharged/)
- Planet KDE: [Update: xdg-toplevel-drag merged](https://blog.david-redondo.de/kde/wayland/qt/2024/02/06/xdg-toplevel-drag-update.html)
- @nullagent: [Good god, linux on phones is getting performant af now 😲There's still bugs, and times when I don't want all these animations if it costs battery. But damn, plasma mobile is starting to feel almost daily usable. It's definitely nicer to look at than my typical bearbones phosh. 🤳🏿@mobian #pinephonepro #linuxmobile #plasmamobile  #debian](https://partyon.xyz/@nullagent/111908744377068999)

#### Sxmo (Userscript corner)
- @pocketvj@fosstodon.org: [found a nice image editor which handles crop nice and fast on #linuxmobileViewnior https://github.com/hellosiyan/Viewniorif some with #C skills would give it some love e.g. dark-mode, multitouch support for zoom&amp;pan, it could become THE mobile image editor.#linux #viewnior](https://fosstodon.org/@pocketvj/111908343666061143)
- @pocketvj@fosstodon.org: [added a countdown for the screenrecorder, so you have time to prepare... 🤓#sxmo #linuxmobilesince my new script-set helps so much on coding on the phone (especially the ocr tool), i can not stop creating new $hit 😅](https://fosstodon.org/@pocketvj/111906266017223366)
- @pocketvj@fosstodon.org: [last script for today...😇record a selection of your screen to a *.gifthis might come in handy for usermanuals or bugreports.#linuxmobile #sxmo](https://fosstodon.org/@pocketvj/111903234757402703)
- @pocketvj@fosstodon.org: [ever needed to scan a qr-code on your mobile phone?you no longer have to ask a friend to scan your screen. we now have qr_roi.sh to do this for us in #sxmo #pmOS #postmarketos on #linuxmobile it scans, displays the content in a popup and copies it to the clipboard 🤓](https://fosstodon.org/@pocketvj/111901960029221382)
- @pocketvj@fosstodon.org: [sometimes text selection just sucks on #linuxmobile or you have an idiotic wifi password on a picture and as a legastenic adhs kid you never type it right..here the solution:https://codeberg.org/magdesign/sxmo-userscripts/src/branch/main/ocr_roi.shmy screencapture-to-ocr-to-clipboard tool 😎#sxmo #postmarketos #wayland #linux](https://fosstodon.org/@pocketvj/111901102120972535)
- @pocketvj@fosstodon.org: [#linuxmobile #sxmo #postmarketosbored of typing all the commands to enable GPS all the time?Grab the userscript and enable GPS with a single click 🤓https://codeberg.org/magdesign/sxmo-userscripts/src/branch/main/gps_enable.sh](https://fosstodon.org/@pocketvj/111900210193865750)
- @pocketvj@fosstodon.org: [want to know your battery status on #sxmo #postmarketos #linuxmobile Here you go!(committed them to sxmo userscripts, but git commit via email and vim makes me headaches. I have no idea where this stuff went and what is happening...)However, grab it here when its fresh:https://codeberg.org/magdesign/sxmo-userscripts/](https://fosstodon.org/@pocketvj/111897408911517504)
- @pocketvj@fosstodon.org: [finally a #sxmo userscript to toggle ssh on/off on #postmarketos #linuxmobile it will ask for the sudo privileges.copy it to: ~/.config/sxmo/userscripts/ssh_toggle.shand make it executable...get it here:https://codeberg.org/magdesign/sxmo-userscripts](https://fosstodon.org/@pocketvj/111895865689936206)

#### Sailfish OS
- Community News - Sailfish OS Forum: [Sailfish Community News, 8th February - FOSDEM'24](https://forum.sailfishos.org/t/sailfish-community-news-8th-february-fosdem24/17950)
- flypig's Gecko log: [Day 152](https://www.flypig.co.uk/list?list_id=1004&list=gecko)
- flypig's Gecko log: [Day 151](https://www.flypig.co.uk/list?list_id=1003&list=gecko)
- flypig's Gecko log: [Day 150](https://www.flypig.co.uk/list?list_id=1002&list=gecko)
- flypig's Gecko log: [Day 149](https://www.flypig.co.uk/list?list_id=1000&list=gecko)
- flypig's Gecko log: [Day 148](https://www.flypig.co.uk/list?list_id=999&list=gecko)
- #sailfishOS: [Would love it if #SailfishOS could take off, but in the meantime check out Jolla's survey (please dont let their form stop you from filling it out)https://forms.office.com/pages/responsepage.aspx?id=YhXNthKVj0ijZDTUZVTJapaBShyBVwJFgA1jpH9hGKpUQlFZOUk3QVo3UExKUkM1Nk9JV0pLR0FaWC4u#Jolla](https://union.place/@marxistvegan/111902031639026554)

#### Ubuntu Touch
- UBports News: [Devices Webpage Update](http://ubports.com/blog/ubports-news-1/post/devices-webpage-update-3914)
- UBports - Development - Issues: [Connect to WPA2/WPA3 and only WPA3 not working](https://gitlab.com/ubports/development/core/lomiri-settings-components/-/issues/31)
- #UbuntuTouch: [Finally took time to flash #ubuntutouch 20.04 to my @PINE64 pinephone braveheart. It was previously  running on the 16.04 version. It works well, no bug so far, but is quite slow. I guess it's expected for this device. I am using it to write this message right now.Feels great to run a 6.5 kernel on a phone too!@ubports](https://mastodon.social/@fla/111909665606414819)
- #UbuntuTouch: [#Blind people here who ever tried to use a #QML #Felgo app and can tell me how accessible it was? There even is a dev app at the app store to test UI widgets in case you read this and would like to help out.Same question I‘d like to ask blind users of #UbuntuTouch. Thanks for boosts, thanks in advance! #A11y](https://fosstodon.org/@lazarus/111902143596633023)

#### Distributions
- postmarketOS Blog: [postmarketOS in 2024-02: More trusted contributors](https://postmarketos.org/blog/2024/02/07/more-tcs/)
- Breaking updates in pmOS edge: [osk-sdl removed from pmaports](https://postmarketos.org/edge/2024/02/08/drop-osk-sdl/)
  - postmarketOS (Mastodon): [osk-sdl waved for the last time in postmarketOS edge 👋 https://postmarketos.org/edge/2024/02/08/drop-osk-sdl/#RemoveTechnicalDebt](https://fosstodon.org/@postmarketOS/111892432193395357)
- Breaking updates in pmOS edge: [Possible issues with new GTK 4.0 renderer on some devices](https://postmarketos.org/edge/2024/02/05/gtk4-new-renderers/)
- postmarketOS pmaports issues: [Provide an easy way for a user to obtain logs from a device that boots to the...](https://gitlab.com/postmarketOS/pmaports/-/issues/2586)
- postmarketOS pmaports Merge Requests: [clockworkpi-uconsole: new device](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/4751)
- postmarketOS Wiki New Pages: [Generic-x86 64](https://wiki.postmarketos.org/wiki/Generic-x86_64)
- postmarketOS Wiki Recent Changes: [User:Knuxfanwin8/Installation guide rework](https://wiki.postmarketos.org/index.php?title=User%3AKnuxfanwin8%2FInstallation_guide_rework&diff=55710&oldid=55708)

#### Apps
- Martijn Braam: [Megapixels 1.8.0 is released](https://fosstodon.org/@martijnbraam/111890710778192535)
- LinuxPhoneApps.org: Apps: [Forge Sparks](https://linuxphoneapps.org/apps/com.mardojai.forgesparks/)
- LinuxPhoneApps.org: Apps: [Door Knocker](https://linuxphoneapps.org/apps/xyz.tytanium.doorknocker/)
- LinuxPhoneApps.org: Apps: [Zodiac](https://linuxphoneapps.org/apps/io.github.alexkdeveloper.zodiac/)
- LinuxPhoneApps.org: Apps: [Warehouse](https://linuxphoneapps.org/apps/io.github.flattool.warehouse/)
- LinuxPhoneApps.org: Apps: [Whakarere](https://linuxphoneapps.org/apps/com.mudeprolinux.whakarere/)
- LinuxPhoneApps.org: Apps: [Shortcut](https://linuxphoneapps.org/apps/io.github.andreibachim.shortcut/)
- LinuxPhoneApps.org: Apps: [unlockR](https://linuxphoneapps.org/apps/com.github.jkotra.unlockr/)
- LinuxPhoneApps.org: Apps: [Muzika](https://linuxphoneapps.org/apps/com.vixalien.muzika/)
- LinuxPhoneApps.org: Apps: [Jupii](https://linuxphoneapps.org/apps/net.mkiol.jupii/) _Nice to see another SailfishOS app ported to Kirigami!_
- LinuxPhoneApps.org: Apps: [Parabolic](https://linuxphoneapps.org/apps/org.nickvision.tubeconverter/)
- LinuxPhoneApps.org: Apps: [Butler](https://linuxphoneapps.org/apps/com.cassidyjames.butler/)
- LinuxPhoneApps.org: Apps: [LocalSend](https://linuxphoneapps.org/apps/org.localsend.localsend_app/)
- LinuxPhoneApps.org: Apps: [Eeman](https://linuxphoneapps.org/apps/sh.shuriken.eeman/)
- LinuxPhoneApps.org: [New Apps of LinuxPhoneApps.org, Q4/2023: 21 new apps, and a simpler way to add new apps!](https://linuxphoneapps.org/blog/new-listed-apps-q4-2023/)

#### Kernel
- phone-devel: [[PATCH v3] ARM: dts: qcom: msm8974: correct qfprom node size](http://lore.kernel.org/phone-devel/20240210-msm8974-qfprom-v3-1-26c424160334@z3ntu.xyz/)
- phone-devel: [[PATCH v2 0/3] Add RPMPD support for MSM8974](http://lore.kernel.org/phone-devel/20240210-msm8974-rpmpd-v2-0-595e2ff80ea1@z3ntu.xyz/)
- Phoronix: [Qualcomm Hardware Support Increasingly In Good Shape On The Mainline Linux Kernel](https://www.phoronix.com/news/Qualcomm-Mainline-Linux-2024)

An anonymous contributor notes on the [pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit): _Some context information I deem important is, that changes to SC7280 also affect QCM6490, since SC7280 is used as the base for QCM6490 - the Fairphone 5 SoC. Since this is not immediately obvious to people not following the kernel, I'd like to somehow have this mentioned, when SC7280 is receiving future work. And venus on Fairphone 5 is kind of a big deal, since we're talking about video encoding/decoding acceleration here._

Additional note by Peter: _Fun fact: [The next ShiftPhone is also going to use QCM6490](https://www.shift.eco/en/shiftphone-8-status-page-2/)._


#### Stack
- Phoronix: [Etnaviv NPU Driver Further Boosts Performance, Striking Closer To The Proprietary Driver](https://www.phoronix.com/news/Etnaviv-NPU-Weight-Compress)
- Phoronix: [2024 Vulkan Developer Conference Slides Posted](https://www.phoronix.com/news/Vulkanised-2024-Slides)
- Phoronix: [Fwupd 1.9.13 Adds Support For Newer Acer USB Docks](https://www.phoronix.com/news/Fwupd-1.9.13-Released)
- Phoronix: [Arm Lands Mali Gen10 "Panthor" Firmware Blob In Linux-Firmware.Git](https://www.phoronix.com/news/Arm-Mali-Panthor-Firmware-Git)

#### Matrix
- Matrix.org: [This Week in Matrix 2024-02-09](https://matrix.org/blog/2024/02/09/this-week-in-matrix-2024-02-09/)
- Matrix.org: [FOSDEM 2024 Wrap Up](https://matrix.org/blog/2024/02/fosdem-wrap-up/)

### Worth Noting
* [Oliver Smith: "tagged pmbootstrap 2.2.0 - straight from the hackathon …"](https://fosstodon.org/@ollieparanoid/111887633535207961)
- postmarketOS (Mastodon): [about to watch the best movie of all timeThe #GNOME mobile (1967)reviews incoming 🛩️](https://fosstodon.org/@postmarketOS/111886441429318021)
- PINE64official (Reddit): [Is PINE64 dead?](https://www.reddit.com/r/PINE64official/comments/1alt9kz/is_pine64_dead/)
- PINE64official (Reddit): [[Tutorial] Wifi and Bluetooth on PineTab 2](https://www.reddit.com/r/PINE64official/comments/1akjlwu/tutorial_wifi_and_bluetooth_on_pinetab_2/)
- Purism community: [Questions about alternate modem_QUECTEL EM060K-GL](https://forums.puri.sm/t/questions-about-alternate-modem-quectel-em060k-gl/22638)
- Purism community: [Librem 5 Docked Mode Graphics](https://forums.puri.sm/t/librem-5-docked-mode-graphics/22636)
- Purism community: [Serious Issues in Desktop Mode](https://forums.puri.sm/t/serious-issues-in-desktop-mode/22634)
- @dos@librem.one: [FWIW I&apos;ve just posted subtitles for the talk, available under the same link above. You can use this mpv invocation below to watch it captioned:```mpv https://video.fosdem.org/2024/h1309/fosdem-2024-3200-universal-serial-bug-a-tale-of-spontaneous-modem-resets.av1.webm --sub-file=https://dosowisko.net/fosdem24/fosdem-2024-3200-universal-serial-bug-a-tale-of-spontaneous-modem-resets.av1.vtt```](https://social.librem.one/@dos/111902433138991685)
- Lemmy - linuxphones: [How is Ubuntu Touch in 2024?](https://lemmy.ml/post/11696032)
- @cas: [woohoo, my #FOSDEM talk "U-Boot for modern Qualcomm Phones" is now up! So if you missed it live now you can find it here.https://fosdem.org/2024/schedule/event/fosdem-2024-1716-u-boot-for-modern-qualcomm-phones/#U-Boot #Linux #LinuxMobile #Qualcomm](https://social.treehouse.systems/@cas/111896113746867662)
- @awai: [Doing some evening hacking, bringing up the @PINE64 #PineTabV with upstream Linux (6.8-rc3) with only a new DTS and no additional patch!So far it boots  with working console output and eMMC/µSD, but that's about it...#LinuxMobile #LinuxOnMobile #MobileLinux #RISCV](https://fosstodon.org/@awai/111892985140450246)
- @nullagent: [Alright, got the new rfpartyd daemon running on the pinephone pro 🤘🏿https://github.com/datapartyjs/rfparty-monitor/pull/2I should probably actually try and get location from the default location provider in mobian. As it is right now I'm doing some hacks to get the modem in the right state to talk to gpsd. I think I missed something on how this supposed to work 🤔Anyway, we get BLE data and when gpsd is happy we'll get location too 😃@rfparty @dataparty @PINE64 @mobian #devlog #mobilelinux #wardriving #cybersecurty](https://partyon.xyz/@nullagent/111899797581016509)
- @awai: [Doing some evening hacking, bringing up the @PINE64 #PineTabV with upstream Linux (6.8-rc3) with only a new DTS and no additional patch!So far it boots  with working console output and eMMC/µSD, but that's about it...#LinuxMobile #LinuxOnMobile #MobileLinux #RISCV](https://fosstodon.org/@awai/111892985140450246)
- @pmOS_devices: [We fixed our postmarketOS device counter page here as well: https://tuxphones.com/static/counter/  #linux #postmarketos #mobilelinux](https://mastodon.social/@pmOS_devices/111884502100114896)
- Asteroid OS: [AsteroidOS at FOSDEM 2024 - AsteroidOS](https://asteroidos.org/news/fosdem-2024/)

### Worth Reading
- fossphones: [Linux Phone News - February 10, 2024](https://fossphones.com/02-10-24.html)
- dcz: [State of input method](https://dorotac.eu/posts/input_broken/)
- Volker Krause: [FOSDEM 2024 and Open Public Transport Routing](https://www.volkerkrause.eu/2024/02/10/fosdem-2024-open-public-transport-routing.html). _Awesomw!_
- LinuxPhoneApps.org: [New Apps of LinuxPhoneApps.org, Q4/2023: 21 new apps, and a simpler way to add new apps!](https://linuxphoneapps.org/blog/new-listed-apps-q4-2023/)
- ramcq: [Flathub: Pros and Cons of Direct Uploads](https://ramcq.net/2024/02/06/flathub-pros-and-cons-of-direct-uploads/)
- hughsie: [fwupd: Auto-Quitting On Idle, Harder](https://blogs.gnome.org/hughsie/2024/02/05/fwupd-auto-quitting-on-idle-harder/)
- Jozef Mlich: [FOSDEM 2024](https://blog.mlich.cz/2024/02/fosdem-2024/)
- LINux on MOBile: [FOSDEM 2024: A Short Report](https://linmob.net/fosdem-2024-a-short-report/)
- Phoronix: [Mozilla Has A New CEO To Focus On The Future](https://www.phoronix.com/news/Mozilla-New-CEO-2024)
- GabMus Dev Log: [Command line options with Relm4](https://gabmus.org/posts/command_line_options_with_relm4/)

#### Marketing Corner
- Purism: [Purism Differentiator Series, Part 3: Operating System](https://puri.sm/posts/purism-differentiator-series-part-3-operating-system/)
- Purism: [$99/mo Librem 5 + Librem AweSIM](https://puri.sm/posts/99-mo-librem-5-librem-awesim/)
- Purism: [Purism Differentiator Series, Part 2: Formation](https://puri.sm/posts/purism-differentiator-series-part-2-formation/)
- Purism: [Purism Differentiator Series, Part 1: Summary](https://puri.sm/posts/purism-differentiator-series-part-1-summary/)

### Worth Watching
- RTP Tech Tips: [💡 📡 Get WiFi Working On Pinetab 2!](https://www.youtube.com/watch?v=JO3rmK4S8Rc)
- Continuum Gaming: [Continuum Gaming E403: SFOS Rumors from FOSDEM](https://www.youtube.com/watch?v=hG2pCcZAC_0)
- [Samsung Z Flip5: running Maemo Leste (vía proot + termux-x11)](https://www.youtube.com/shorts/FFkPyAtxS5M)

#### FOSDEM 2024

Let's start with FOSS on Mobile:

- [FOSDEM 2024 - From phone hardware to mobile Linux](https://fosdem.org/2024/schedule/event/fosdem-2024-2234-from-phone-hardware-to-mobile-linux/)
- [FOSDEM 2024 - U-Boot for modern Qualcomm phones](https://fosdem.org/2024/schedule/event/fosdem-2024-1716-u-boot-for-modern-qualcomm-phones/)
- [FOSDEM 2024 - Mainline Linux on Qualcomm SoCs, are we here now ?](https://fosdem.org/2024/schedule/event/fosdem-2024-1707-mainline-linux-on-qualcomm-socs-are-we-here-now-/)
- [FOSDEM 2024 - Droidian - Bridging the gap between various platforms with convergence](https://fosdem.org/2024/schedule/event/fosdem-2024-3165-droidian-bridging-the-gap-between-various-platforms-with-convergence/)
- [FOSDEM 2024 - Genode on the PinePhone on track to real-world usability](https://fosdem.org/2024/schedule/event/fosdem-2024-3017-genode-on-the-pinephone-on-track-to-real-world-usability/)
- [FOSDEM 2024 - Wayland's input-method is broken and it's my fault](https://fosdem.org/2024/schedule/event/fosdem-2024-2972-wayland-s-input-method-is-broken-and-it-s-my-fault/)
- [FOSDEM 2024 - Why not run OpenCL-accelerated LLM on your phone?](https://fosdem.org/2024/schedule/event/fosdem-2024-3364-why-not-run-opencl-accelerated-llm-on-your-phone-/)
- [FOSDEM 2024 - Daily blogging embedded Gecko development](https://fosdem.org/2024/schedule/event/fosdem-2024-2508-daily-blogging-embedded-gecko-development/)

Luca's big talk:

- [FOSDEM 2024 - Open Source for Sustainable and Long lasting Phones](https://fosdem.org/2024/schedule/event/fosdem-2024-3362-open-source-for-sustainable-and-long-lasting-phones/)

And some other interesting talks:

- [FOSDEM 2024 - Enhancing Linux Accessibility: A Unified Approach](https://fosdem.org/2024/schedule/event/fosdem-2024-2949-enhancing-linux-accessibility-a-unified-approach/)
- [FOSDEM 2024 - A fully open source stack for MIPI cameras](https://fosdem.org/2024/schedule/event/fosdem-2024-3013-a-fully-open-source-stack-for-mipi-cameras/)
- [FOSDEM 2024 - [StructuredEmail] When is my flight? - Semantic data extraction in KMail and Nextcloud Mail](https://fosdem.org/2024/schedule/event/fosdem-2024-2053--structuredemail-when-is-my-flight-semantic-data-extraction-in-kmail-and-nextcloud-mail/)
- [FOSDEM 2024 - Privacy-respecting usage metrics for free software projects](https://fosdem.org/2024/schedule/event/fosdem-2024-3648-privacy-respecting-usage-metrics-for-free-software-projects/)
- [FOSDEM 2024 - From Kernel API to Desktop Integration, how do we integrate battery charge limiting in the desktop](https://fosdem.org/2024/schedule/event/fosdem-2024-2123-from-kernel-api-to-desktop-integration-how-do-we-integrate-battery-charge-limiting-in-the-desktop/)
- [FOSDEM 2024 - The state of video offloading on the Linux Desktop](https://fosdem.org/2024/schedule/event/fosdem-2024-3557-the-state-of-video-offloading-on-the-linux-desktop/)
- [FOSDEM 2024 - GStreamer: State of the Union 2024](https://fosdem.org/2024/schedule/event/fosdem-2024-3590-gstreamer-state-of-the-union-2024/)
- [FOSDEM 2024 videos](https://marcin.juszkiewicz.com.pl/download/tables/fosdem/)
- [FOSDEM 2024 - PipeWire State of the Union](https://fosdem.org/2024/schedule/event/fosdem-2024-1988-pipewire-state-of-the-union/)
- [FOSDEM 2024 - Take Your FOSS Project From Surviving To Thriving](https://fosdem.org/2024/schedule/event/fosdem-2024-2741-take-your-foss-project-from-surviving-to-thriving/)
- [FOSDEM 2024 - MatrixRTC: The Future of Matrix Calls](https://fosdem.org/2024/schedule/event/fosdem-2024-2876-matrixrtc-the-future-of-matrix-calls/)
- [FOSDEM 2024 - The state of the Matrix Rust SDK in 2023](https://fosdem.org/2024/schedule/event/fosdem-2024-3283-the-state-of-the-matrix-rust-sdk-in-2023/)
- [FOSDEM 2024 - The Matrix State of the Union](https://fosdem.org/2024/schedule/event/fosdem-2024-3285-the-matrix-state-of-the-union/)
- [FOSDEM 2024 - Flutter, Buildroot, and you!](https://fosdem.org/2024/schedule/event/fosdem-2024-2495-flutter-buildroot-and-you-/)
- [FOSDEM 2024 - Modos: Building an Ecosystem of Open-Hardware E Ink Devices](https://fosdem.org/2024/schedule/event/fosdem-2024-3052-modos-building-an-ecosystem-of-open-hardware-e-ink-devices/)

### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

