+++
title = "Mer - again!"
aliases = ["2011/10/mer-again.html"]
date = "2011-10-04T18:43:00Z"
[taxonomies]
tags = ["Android", "community efforts", "EFL", "GTK+", "HTML 5", "LiMo", "Maemo", "MeeGo", "Mer", "Qt", "Tizen"]
categories = ["software", "commentary"]
authors = ["peter"]
+++
Remember Mer? That attempt to build a free Maemo distribution by replacing the closed source parts of Maemo (yes, there were some), which was halted when MeeGo was introduced? Remember that Mer stood for &#8220;Maemo Reconstructed? Well, if you don't remember that, I didn't remember the second part, either.

Now that MeeGo is, say, abandoned, Mer is live again. 
<!-- more -->
It's a little complicated to understand all this. First of all, there wasn't just one &#8220;MeeGo&#8221; distribution, there were at least two&mdash;the open source project, Nokias MeeGo (Harmattan (N9, N950)) and so on. Talking of releases, this becomes a mess, so I don't want to go into detail.

Now that Intel, The Linux Foundation and the LiMo Foundation (that backed the MeeGo competitor &#8220;LiMo&#8221; before, which is better defined as a set of technologies or a middleware than as a mobile OS (unlike Android, ..)) moved on to build something new again, which they call &#8220;Tizen&#8221;, there are (naturally) some MeeGo developers - or to put it differently&mdash;some persons in the MeeGo community, that don't really like this move (towards HTML5, towards yet another platform initiative) and want to go on with MeeGo. For all these people, Mer is a vehicle.

As I may be not all correct with this post, I highly recommend you to read <a href="http://www.mail-archive.com/meego-dev@meego.com/msg10749.html">Carsten Munks Mailing List post</a>.

Make sure to check out <a href="http://mer-l-in.blogspot.com/2011/08/restructure-meego-by-installments.html">this Mer blog</a>, too.
