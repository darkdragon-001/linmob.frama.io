+++
title = "Videos" 
description = "This page lists all videos I created for my LINMOB channels about Linux on Smartphones"
path = "videos" 
template = "single.html"
updated = 2024-03-14
[extra]
update_note = "Added links to two new videos."
+++

This page lists all my videos about Linux on Smartphones.


## By Distribution

### Arch Linux ARM/DanctNIX mobile

* PinePhone Software Progress: Phosh's App Drawer, Video Acceleration, Kasts and more (July 30th, 2021): [Odysee](https://odysee.com/@linmob:3/pinephone-software-progress-phosh's-app:3), [YouTube](https://www.youtube.com/watch?v=C4RC3Miuo2A).
* DanctNIX Mobile/Arch Linux ARM for PinePhone and LINMOBapps (October 24th, 2020): [Odysee](https://odysee.com/@linmob:3/danctnix-mobile-arch-linux-arm-for:e), [YouTube](https://www.youtube.com/watch?v=dxup2c9aNzE).
* PinePhone: Installing Anbox on Arch Linux ARM, trying WhatsApp and Signal (September 6th, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-installing-anbox-on-arch-linux:f), [YouTube](https://www.youtube.com/watch?v=Y-9Wmki7DsU).
* PinePhone Convergence with Phosh and Editing .desktop files (November 5th, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-convergence-with-phosh-and:f), [YouTube](https://www.youtube.com/watch?v=y9pOCXy0Q94).


### AVMultiPhone

* AVMultiPhone - (MATE) Desktop Linux on the PinePhone (August 23rd, 2020): [Odysee](https://odysee.com/@linmob:3/avmultiphone-mate-desktop-linux-on-the:3), [YouTube](https://www.youtube.com/watch?v=UNaftRCCRP8).

### Fedora

* Fedora on the PinePhone: Pipewire calling! (February 4th, 2021): [Odysee](https://odysee.com/@linmob:3/fedora-on-the-pinephone-pipewire-calling:1), [YouTube](https://www.youtube.com/watch?v=TcLnC74Vo7c).


### Gentoo

* :( (No video yet.)

### GloDroid

* Android on the PinePhone: Installing GloDroid (June 1st, 2021): [Odysee](https://odysee.com/@linmob:3/android-on-the-pinephone-installing:9), [YouTube](https://www.youtube.com/watch?v=ZdmBq0rFPa8).

### KDE Neon

* The current state of Plasma Mobile on the PinePhone (October 23rd, 2020): [Odysee](https://odysee.com/@linmob:3/the-current-state-of-plasma-mobile-on:3), [YouTube](https://www.youtube.com/watch?v=Pz9DgFRmOMQ).
* KDE Neon Plasma Mobile on the PinePhone (July 28th, 2020): [Odysee](https://odysee.com/@linmob:3/kde-neon-plasma-mobile-on-the-pinephone:b), [YouTube](https://www.youtube.com/watch?v=oVcYaCgwN-U).

### LuneOS

* webOS in 2020 - LuneOS on the PinePhone (August 22th, 2020): [Odysee](https://odysee.com/@linmob:3/webos-in-2020-luneos-on-the-pinephone:c), [YouTube](https://www.youtube.com/watch?v=IQB1QACgZOU).

### Maemo Leste

* Best of LINMOB Live #1: Maemo Leste on the PinePhone (December 4th, 2020): [Odysee](https://odysee.com/@linmob:3/best-of-limmob-live-1-maemo-leste-on-the:a), [YouTube](https://www.youtube.com/watch?v=xN5HTNcWyL0).
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).

### Manjaro Lomiri

* Manjaro Daily Dev Builds for PinePhone - a look at Lomiri, Phosh and Plasma Mobile (February 19th, 2021): [Odysee](https://odysee.com/@linmob:3/manjaro-daily-dev-builds-for-pinephone-a:1), [YouTube](https://www.youtube.com/watch?v=cW224e-4ZYE).
* Another look at Manjaro Lomiri on the PinePhone (January 12th, 2021): [Odysee](https://odysee.com/@linmob:3/another-look-at-manjaro-lomiri-on-the:c), [YouTube](https://www.youtube.com/watch?v=nGU6_Lxjw0k).
* PinePhone: Manjaro Lomiri Alpha 2 (November 25th, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-manjaro-lomiri-alpha-2:0), [YouTube](https://www.youtube.com/watch?v=KJHn-Oec-YI).
* Manjaro Lomiri Alpha 1 on the PinePhone (September 20th, 2020): [Odysee](https://odysee.com/@linmob:3/manjaro-lomiri-alpha-1-on-the-pinephone:9), [YouTube](https://www.youtube.com/watch?v=sOinLw0xP3c).


### Manjaro Phosh

* Manjaro Daily Dev Builds for PinePhone - a look at Lomiri, Phosh and Plasma Mobile (February 19th, 2021): [Odysee](https://odysee.com/@linmob:3/manjaro-daily-dev-builds-for-pinephone-a:1), [YouTube](https://www.youtube.com/watch?v=cW224e-4ZYE).
* Manjaro Phosh Alpha 4: Anbox Community Service (Anbox on PinePhone III) (September 30th, 2020): [Odysee](https://odysee.com/@linmob:3/manjaro-phosh-alpha-4-anbox-community:3), [YouTube](https://www.youtube.com/watch?v=8Sha3R4PKSs).
* PinePhone: Manjaro Phosh Alpha 4 Walkthrough + GNOME 3.38 App improvements (September 23rd, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-manjaro-phosh-alpha-4:b), [YouTube](https://www.youtube.com/watch?v=VefFyTF3c-I).


### Manjaro Plasma Mobile

* Unboxing the PinePhone Pro Explorer Edition (Febuary 1st, 2022):  [Odysee](https://odysee.com/@linmob:3/unboxing-the-pinephone-pro-explorer:1), [YouTube](https://www.youtube.com/watch?v=h6C2UatW8tQ).
* Manjaro Daily Dev Builds for PinePhone - a look at Lomiri, Phosh and Plasma Mobile (February 19th, 2021): [Odysee](https://odysee.com/@linmob:3/manjaro-daily-dev-builds-for-pinephone-a:1), [YouTube](https://www.youtube.com/watch?v=cW224e-4ZYE).
* Getting started with the KDE Community Edition PinePhone (January 22th, 2021): [Odysee](https://odysee.com/@linmob:3/getting-started-with-the-kde-community:1), [YouTube](https://www.youtube.com/watch?v=1uUMiNS5rlw).
* PinePhone: Manjaro Plasma Mobile (soon on the KDE Community Edition) (December 2nd, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-manjaro-plasma-mobile-soon-on:d), [YouTube](https://www.youtube.com/watch?v=eJ8V0lRxKgM).
* The current state of Plasma Mobile on the PinePhone (October 23rd, 2020): [Odysee](https://odysee.com/@linmob:3/the-current-state-of-plasma-mobile-on:3), [YouTube](https://www.youtube.com/watch?v=Pz9DgFRmOMQ).
* PinePhone - Manjaro Plasma Mobile Alpha 6 (August 13th, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-manjaro-plasma-mobile-alpha-6:4), [YouTube](https://www.youtube.com/watch?v=Wn7bliZrHJ8).

### Mobian

* Mobian on the Purism Librem 5 Evergreen (March 20th, 2021): [Odysee](https://odysee.com/@linmob:3/mobian-on-the-purism-librem-5-evergreen:f), [YouTube](https://www.youtube.com/watch?v=7_4zCOTELlU).
* Anbox on Mobian on the PinePhone (August 14th, 2020): [Odysee](https://odysee.com/@linmob:3/anbox-on-mobian-on-the-pinephone:d), [YouTube](https://www.youtube.com/watch?v=v06KUrfs69k).
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).
* PinePhone Daily Driver Challenge / Taking pictures on Mobian (July 9th, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-daily-driver-challenge-taking:c), [YouTube](https://www.youtube.com/watch?v=0Ju6szQebPk).



### Nemo Mobile

* Nemo Mobile on the PinePhone (October 29th, 2020): [Odysee](https://odysee.com/@linmob:3/nemo-mobile-on-the-pinephone:6), [YouTube](https://www.youtube.com/watch?v=mKHuZch0kF4).

### Nix OS

* :( (No video yet.)

### OpenMandriva

* :( (No video yet, I am waiting for their second image.)

### openSUSE

* OpenSUSE Plasma Mobile on the PinePhone (April 21st, 2021): [Odysee](https://odysee.com/@linmob:3/opensuse-plasma-mobile-on-the-pinephone:6), [YouTube](https://www.youtube.com/watch?v=eCrGi8hsGhY).
* Revisiting openSUSE on the PinePhone (January 11th, 2021): [Odysee](https://odysee.com/@linmob:3/revisiting-opensuse-on-the-pinephone:e), [YouTube](https://www.youtube.com/watch?v=Zac6WgYIn28).
* slem.os: OpenSUSE on the PinePhone (November 12th, 2020): [Odysee](https://odysee.com/@linmob:3/slem-os-opensuse-on-the-pinephone:a), [YouTube](https://www.youtube.com/watch?v=G8mR52ulDvw).

### postmarketOS Phosh

* Upgrading to postmarketOS 22.06 (Phosh) on PinePhone (June 14th, 2022): [TilVids](https://tilvids.com/w/ohK7QfPp5Ej8Q1Y9GucLCR), [YouTube](https://www.youtube.com/watch?v=zKZj9aQmAjA).
* Updating PinePhone (Pro) Modem Firmware with GNOME Firmware Updater (February 26th, 2022): [TilVids](https://tilvids.com/w/2htMubjLy6Wh6yeX3jaQJa), [Odysee](https://odysee.com/@linmob:3/updating-pinephone-(pro)-modem-firmware:3), [YouTube](https://www.youtube.com/watch?v=IsFbVZsQJX4).
* postmarketOS with Phosh on the bq Aquaris X5 (November 6th, 2020): [Odysee](https://odysee.com/@linmob:3/postmarketos-with-phosh-on-the-bq:c), [YouTube](https://www.youtube.com/watch?v=ZZR3yAamwXA).
* Unboxing the Pine64 PinePhone postmarketOS Community Edition Convergence Package (September 18th, 2020): [Odysee](https://odysee.com/@linmob:3/unboxing-the-pine64-pinephone:6), [YouTube](https://www.youtube.com/watch?v=g2w0p_gzpds).
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).

### postmarketOS Plasma Mobile

* A short look at Plasma Mobile 6 (March 9th, 2024): [TilVids](https://tilvids.com/w/3Z5sc15TxHH8QDpDTZBRUL), [YouTube](https://www.youtube.com/watch?v=qbqd6aJASdI).
* The fastest mainline Linux Phone: Pocophone F1 (March 24th, 2022): [TilVids](https://tilvids.com/w/3mn337s4Mx2WPSHRX3KJsz), [Odysee](https://odysee.com/@linmob:3/the-fastest-mainline-linux-phone:3), [YouTube](https://www.youtube.com/watch?v=KtfTJbLiYfg).
* Installing postmarketOS with Plasma Mobile on the Motorola Moto G4 Play (Harpia) (May 21st, 2021): [Odysee](https://odysee.com/@linmob:3/installing-postmarketos-with-plasma:0), [YouTube](https://www.youtube.com/watch?v=C4LKllQT_GQ).
* The current state of Plasma Mobile on the PinePhone (October 23rd, 2020): [Odysee](https://odysee.com/@linmob:3/the-current-state-of-plasma-mobile-on:3), [YouTube](https://www.youtube.com/watch?v=Pz9DgFRmOMQ).
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).

### PureOS

* The new gesture-based navigation of Phosh 0.20 on the Librem 5 (June 11th, 2022): [TilVids](https://tilvids.com/w/sZeifcSyyvbYXXzhTts81T), [Odysee](https://odysee.com/@linmob:3/the-new-gesture-based-navigation-of:b), [YouTube](https://www.youtube.com/watch?v=4S7M7fqcjY8). 
* Unboxing another Librem 5 (and a glimpse at LinuxPhoneApps.org) (May 17th, 2022): [TilVids](https://tilvids.com/w/wCx1pREQp9nzVnjasy9D4Y), [Odysee](https://odysee.com/@linmob:3/unboxing-another-librem-5-\(and-a-glimpse:7), [YouTube](https://www.youtube.com/watch?v=A5zyqAGjcSI).
* Librem 5 with PureOS Byzantium: Work in Progress + AppStream Metadata nerdery (May 22nd, 2021): [Odysee](https://odysee.com/@linmob:3/librem-5-with-pureos-byzantium-work-in:a), [YouTube](https://www.youtube.com/watch?v=zEsu9GFGzns).
* Unboxing the Purism Librem 5 Evergreen (January 28th, 2021): [Odysee](https://odysee.com/@linmob:3/unboxing-the-purism-librem-5-evergreen:e), [Youtube](https://www.youtube.com/watch?v=5WdnPw537nU).
* PureOS on the PinePhone? Sure! (December 20th, 2020): [Odysee](https://odysee.com/@linmob:3/pureos-on-the-pinephone-sure:5), [YouTube](https://www.youtube.com/watch?v=oT9XUkui4zs).
* SuperTuxKart on PureOS on the PinePhone (December 20th, 2020): DevTube (gone).
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).

### Sailfish OS

* Sailfish OS 4.0.1.48 on the PinePhone (March 5th, 2021): [Odysee](https://odysee.com/@linmob:3/sailfish-os-4.0.1.48-on-the-pinephone:9), [YouTube](https://www.youtube.com/watch?v=DcdNwlmG1yk).
* Sailfish OS on the PinePhone (October 18th, 2020): [Odysee](https://odysee.com/@linmob:3/sailfish-os-on-the-pinephone:0), [YouTube](https://www.youtube.com/watch?v=hZevZqXlNXc).
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).

### Sxmo

* How I started using the PinePhone Pro after giving up on it + FOSDEM 2024 preview (January 6th, 2024): [TilVids](https://tilvids.com/w/eYyhq5SJcY9DzF3VSpUE5w).
* Sxmo: Simple X Mobile on PinePhone (November 12th, 2020): [Odysee](https://odysee.com/@linmob:3/sxmo-simple-x-mobile-on-pinephone:5), [YouTube](https://www.youtube.com/watch?v=c0-JatWCDuo).
* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).

### Ubuntu Touch

* Looking at some PinePhone distributions and a closer look at postmarketOS (June 25th, 2020): [Odysee](https://odysee.com/@linmob:3/looking-at-some-pinephone-distributions:a), [YouTube](https://www.youtube.com/watch?v=JzazgbL4u6U).
* PinePhone UBPorts Community Edition Unboxing (June 9th, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-ubports-community-edition:1), [YouTube](https://www.youtube.com/watch?v=uhx7XelZuZk).

## Topical videos [^1]

### Anbox

* Manjaro Phosh Alpha 4: Anbox Community Service (Anbox on PinePhone III) (September 30th, 2020): [Odysee](https://odysee.com/@linmob:3/manjaro-phosh-alpha-4-anbox-community:3), [YouTube](https://www.youtube.com/watch?v=8Sha3R4PKSs).
* PinePhone: Installing Anbox on Arch Linux ARM, trying WhatsApp and Signal (September 6th, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-installing-anbox-on-arch-linux:f), [YouTube](https://www.youtube.com/watch?v=Y-9Wmki7DsU).
* Anbox on Mobian on the PinePhone (August 14th, 2020): [Odysee](https://odysee.com/@linmob:3/anbox-on-mobian-on-the-pinephone:d), [YouTube](https://www.youtube.com/watch?v=v06KUrfs69k).

### Convergence

#### Plasma Mobile
* Manjaro Plasma Mobile on the PinePhone: Convergence (Dev build 201107) (November 10th, 2020): [Odysee](https://odysee.com/@linmob:3/manjaro-plasma-mobile-on-the-pinephone:e), [YouTube](https://www.youtube.com/watch?v=OgEnSd5w4cQ).

#### Phosh
* PinePhone Convergence with Phosh and Editing .desktop files (November 5th, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-convergence-with-phosh-and:f), [YouTube](https://www.youtube.com/watch?v=y9pOCXy0Q94).

### PinePhone Accessories
* PinePhone (Pro) External Batteries: PinePhone Keyboard vs. 8500 mAh S 20 Ultra Battery case (February 23rd, 2022): [TilVids](https://tilvids.com/w/w3qXbr31SE21jGxnM8A1r7), [Odysee](https://odysee.com/@linmob:3/pinephone-(pro)-external-batteries:2), [YouTube](https://www.youtube.com/watch?v=M5T_Wol-tHQ).

### Fake-Convergence: Running proper desktops on the Phone while it's connected

#### GNOME
* Best of LINMOB Live #1: postmarketOS GNOME on the PinePhone (December 3rd, 2020): [Odysee](https://odysee.com/@linmob:3/best-of-limmob-live-1-postmarketos-gnome:0), [YouTube](https://www.youtube.com/watch?v=1TZm-ZPlToE).

#### Plasma Desktop
* PinePhone: Plasma Desktop and the Convergence Dock (December 5th, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-plasma-desktop-and-the:2), [YouTube](https://www.youtube.com/watch?v=f2Mmd3gRaTs).


### Flashing the Phone 
* PinePhone: Flashing the PinePhone from the PinePhone (December 28th, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-flashing-the-pinephone-from:a), [YouTube](https://www.youtube.com/watch?v=VPV024bRZ_4)

### Flashing Modem Firmware
* Updating PinePhone (Pro) Modem Firmware with GNOME Firmware Updater (February 26th, 2022): [TilVids](https://tilvids.com/w/2htMubjLy6Wh6yeX3jaQJa), [Odysee](https://odysee.com/@linmob:3/updating-pinephone-(pro)-modem-firmware:3), [YouTube](https://www.youtube.com/watch?v=IsFbVZsQJX4).

### Firefox: Advanced Configuration
* Firefox Profiles and Site Specific Browsers on the PinePhone (January 9th, 2021): [Odysee](https://odysee.com/@linmob:3/firefox-profiles-and-site-specific:c), [YouTube](https://www.youtube.com/watch?v=h7aLE8jBOPc).


### Launcher clean-up (removing non-needed launchers)

* PinePhone Convergence with Phosh and Editing .desktop files (November 5th, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-convergence-with-phosh-and:f), [YouTube](https://www.youtube.com/watch?v=y9pOCXy0Q94).

### Getting started
* Getting started with the KDE Community Edition PinePhone (January 22th, 2021): [Odysee](https://odysee.com/@linmob:3/getting-started-with-the-kde-community:1), [YouTube](https://www.youtube.com/watch?v=1uUMiNS5rlw).

### Maps and Navigation

* PinePhone Daily Driver Challenge - Maps and Navigation (July 17th, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-daily-driver-challenge-maps:3), [YouTube](https://www.youtube.com/watch?v=WvGNXBntkp0).

### Qt/Plasma Mobile apps on Phosh (mixing worlds)

* PinePhone DDC - Qt/Plasma Mobile Apps on Phosh (August 1st, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-ddc-qt-plasma-mobile-apps-on:2), [YouTube](https://www.youtube.com/watch?v=Ka9zh0Vt3zM).

### Unboxings
* Unboxing another Librem 5 (and a glimpse at LinuxPhoneApps.org) (May 17th, 2022): [TilVids](https://tilvids.com/w/wCx1pREQp9nzVnjasy9D4Y), [Odysee](https://odysee.com/@linmob:3/unboxing-another-librem-5-\(and-a-glimpse:7), [YouTube](https://www.youtube.com/watch?v=A5zyqAGjcSI).
* Unboxing the PinePhone Pro Explorer Edition (Febuary 1st, 2022): [Odysee](https://odysee.com/@linmob:3/unboxing-the-pinephone-pro-explorer:1), [YouTube](https://www.youtube.com/watch?v=h6C2UatW8tQ).
* Unboxing the Purism Librem 5 Evergreen (January 28th, 2021): [Odysee](https://odysee.com/@linmob:3/unboxing-the-purism-librem-5-evergreen:e), [Youtube](https://www.youtube.com/watch?v=5WdnPw537nU).
* Unboxing the Pine64 PinePhone postmarketOS Community Edition Convergence Package (September 18th, 2020): [Odysee](https://odysee.com/@linmob:3/unboxing-the-pine64-pinephone:6), [YouTube](https://www.youtube.com/watch?v=g2w0p_gzpds).
* PinePhone UBPorts Community Edition Unboxing (June 9th, 2020): [Odysee](https://odysee.com/@linmob:3/pinephone-ubports-community-edition:1), [YouTube](https://www.youtube.com/watch?v=uhx7XelZuZk).

## Live Stream recordings[^2] 

* LINMOBlive 3: Librem 5 impressions and PinePhone news (February 17th, 2021): [Odysee](https://odysee.com/@linmob:3/linmoblive3-librem5-and-pinephone:4)
* LINMOBlive 2: Manjaro Lomiri and random unboxing (January 12th, 2021): [Odysee](https://odysee.com/@linmob:3/linmoblive-2-manjaro-lomiri-and-random:d).
* LINMOBlive 1: Fun with Megi's 2020/11/23 p-boot-demo image and the Lap Dock (November 30th, 2020): [Odysee](https://odysee.com/@linmob:3/live-1-Playing-with-Megi's-new-p-boot-demo-image:2).


[^1]: Limited to videos that are not just about distributions, but show stuff that might be helpful or focusses on a cross-distribution topic.

[^2]: There have been more Live Streams  (they all happen on YouTube for now) that I did not make available. Future uncut recordings are going to be Odysee exclusives.
