+++
title = "Weekly GNU-like Mobile Linux Update (12/2023): Ubuntu Touch 20.04 OTA 1 and GNOME 44"
draft = false
date = "2023-03-27T21:15:00Z"
[taxonomies]
tags = ["Ubuntu Touch","Sailfish OS","GNOME","Phosh",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Ubuntu Touch finally has a first 20.04 release, a Sailfish Community Update, a glimpse at upcoming Phosh features, and some fun stuff in Worth Noting!

<!-- more -->
_Commentary in italics._


### Software progress

#### Ubuntu Touch
- UBports News: [Ubuntu Touch OTA-1 Focal Release](https://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-1-focal-release-3888) _This was such a massive endeavour! Congrats to everyone who worked on this!_
- UBports News: [Ubuntu Touch OTA-25 Call for Testing](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-25-call-for-testing-3887)
- [UBports: "Testing for OTA-25 has revealed a regression in video playback…"](https://mastodon.social/@ubports/110091023362044234)
- [Codemaster Freders: "Pocket VMs 0.7 is out and will…"](https://mastodon.social/@fredldotme/110083790928445884)

#### GNOME ecosystem
- This Week in GNOME: [#88 Forty-four!](https://thisweek.gnome.org/posts/2023/03/twig-88/)
- Michael Catanzaro: [WebKitGTK API for GTK 4 Is Now Stable](https://blogs.gnome.org/mcatanzaro/2023/03/21/webkitgtk-api-for-gtk-4-is-now-stable/)
- [Guido Günther: "With the groundwork done in #phosh 0.25 and the (upcoming) 0.26 to move power button handling from the compositor (#phoc) to the shell (phosh) we'd have the bits in place to wire up  a long press menu for e.g. emergency calls, screenshots, etc.…"](https://social.librem.one/@agx/110085526355619892)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: Distro upgrades for Fedora KDE in Discover](https://pointieststick.com/2023/03/25/this-week-in-kde-distro-upgrades-for-fedora-kde-in-discover/)
- Nicolas Fella: [Building Plasma against Qt6](https://nicolasfella.de/posts/building-plasma-against-qt6/)
- [Seshan "Seshpenguin": "#PlasmaMobile KF6/Qt6 successfully running on the #PinePhone, which I think is a first for running on real hardware (at least according to @espidev)!…"](https://social.sineware.ca/@seshpenguin/110065691768480138)

#### Sailfish OS
- [Sailfish Community News, 23 March, Hotfixes](https://forum.sailfishos.org/t/sailfish-community-news-23-march-hotfixes/15075)

#### Linux
- Phoronix: [Linux 6.3-rc4 Released: "Looking Pretty Normal"](https://www.phoronix.com/news/Linux-6.3-rc4)

#### Non-Linux
- Lup Yuen: [NuttX RTOS for PinePhone: Simpler USB with EHCI (Enhanced Host Controller Interface)](https://lupyuen.github.io/articles/usb3)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-03-24](https://matrix.org/blog/2023/03/24/this-week-in-matrix-2023-03-24)

### Worth noting
- Still crying about Tootle's archival? [Tuba](https://tuba.geopjr.dev/) (previously known as Tooth) has [just been released](https://github.com/GeopJr/Tuba/releases/tag/v0.1.0), and it's available on [Flathub](https://flathub.org/apps/details/dev.geopjr.Tuba)!
- Always wanted to try Guix on a Phone? Here you go: [\~abcdw/guix-pinephonepro - A minimal Guix System with custom linux kernel for PinePhone Pro - sourcehut git](https://git.sr.ht/~abcdw/guix-pinephonepro) (via [Fosstodon](https://fosstodon.org/@abcdw/110079134145491645)).
- [LinuxPhoneApps.org: "Do you (or did you ever) use any #Snaps on your #MobileLinux/ #LinuxMobile device?…"](https://linuxrocks.online/@linuxphoneapps/110083847770054131) _If you're using a snap/multiple snaps on your Linux Phone, please share a link to it (or multiple)!_
- [Robert Mader: "We recently discovered that the #libcamera + #pipewire stack was actually broken on most distros for devices requiring an IPA - such as the #PinePhonePro and other mobile phones - when using distro-packages and not building either of the two projects from source.…"](https://floss.social/@rmader/110064130131552699)

### Worth reading
- Liliputing: [Ubuntu Touch OTA-1 Focal brings Ubuntu 20.04 LTS to smartphones](https://liliputing.com/ubuntu-touch-ota-1-focal-brings-ubuntu-20-04-lts-to-smartphones/)

### Worth listening
- [Linux-Smartphones - FOCUS ON: Linux - Podcast](https://focusonlinux.podigee.io/50-linux-smartphones). _German._

### Worth watching
- such deviant: [Cutting into the PINEPHONE keyboard!](https://www.youtube.com/watch?v=UvPrwqqLJm4)
- qkall: [SXMO Arch March 2023 - Pinephone Pro](https://www.youtube.com/watch?v=znbwF5XOWfw)
- Canal do Lukita: [WhatsApp on Ubuntu Touch #linuxphone](https://www.youtube.com/watch?v=YYy5iO7P6_o)
- Canal do Lukita: [Trying install Nubank on Ubuntu Touch #linuxphone #linux #ubuntu #nubank](https://www.youtube.com/watch?v=oe9IBHAdjeY)
- Canal do Lukita: [Playing Super Mario on Ubuntu Touch (GBC Emulator) #linuxphone #supermario #ubuntu #nintendo](https://www.youtube.com/watch?v=yzq_6oL1blU)
- Caffeine: [Ubuntu Touch OTA-25 RC improvements update.](https://www.youtube.com/watch?v=fWddH4fy66k)
- Kayuz6: [Sailfish OS 4.5.0.19 new features and brief review!](https://www.youtube.com/watch?v=pOaeOHzT9Ls)
- Continuum Gaming: [E357: Sailfish OS – Minor vs. Major Updates, CLAT and QT 5.15](https://www.youtube.com/watch?v=dD3vYcB9vXw)

### Thanks
Huge thanks again to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

