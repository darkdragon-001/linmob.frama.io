+++
title = "Using the PinePhone Pro daily, despite having given up on it"
date = "2024-01-06T19:24:17Z"
updated = "2024-03-14"
draft = false
[taxonomies]
tags = ["PinePhone Pro", "PinePhone", "PINE64", "PinePhone Keyboard Accessory", "Micro Laptop",]
categories = ["hardware", "personal"]
authors = ["Peter"]
[extra]
update_note = "Corrected first sale date of the PinePhone Keyboard, mention distro in use."
+++


_After my [failure with Beepy](https://linmob.net/enter-beepy-esc/), and
inspired by the [SiPeed Lichee Pi4a Console](https://liliputing.com/lichee-console-4a-is-a-mini-laptop-with-a-risc-v-processor-and-7-inch-display/), I figured: Let's see if a micro-laptop
is still something my aging self finds useful._

<!-- more -->
### Context

Even with a Linux phone, I've carried my Surface Go 2 with me on most occasions - 
just like around 2010, when you would rarely find me without my EeePC. I just
like the comfort of a semi-fullsized keyboard, a large enough screen. While the
Surface Go 2 is a tablet, I rarely use it without the keyboard connected, it's
very much my 2020s netbook.[^1]

Long story short: The Surface Go 2 is almost perfect[^2], but something, that
fits into the pants or jacket pocket would be, if still usable enough, even
more convenient.

Before buying the SiPeed Console, I figured: Let's try that rarely used
PinePhone Pro with the PinePhone Keyboard accessory, which had long been half-
defect (the battery part had stopped working). I fortunately had a ordered a
replacement mainboard for the keyboard months earlier, and guess what:
Exchanging the mainboard was easy enough and fixed the issue. I would finally
use two items that had seen way less use than I had hoped for when purchasing.

### The PinePhone Keyboard

The Keyboard accessory was first sold on the [very last day of 2021](https://www.pine64.org/2021/12/31/happy-new-year-the-keyboard-and-cases-are-here/), and is an
i2c connected keyboard cover featuring a 6000mAh battery. It can be a bit
finicky: The PINs do not always fit perfectly on the first try, at least with
the OG AllWinner A64 powered PinePhone - I have not had any issues with my
PinePhone Pro.

When I first tried it, software support was still problematic: Distributions
did not ship a proper keymap yet. I can't really comment how this is right now - 
DanctNIX does not seem to apply the keymap in my testing, postmarketOS, how
ever does so. There's also more than one approach to being able to type
each and every character: Ship a keymap, enable a service, and who knows what
else. 

I also did not like the key travel and general mushiness - I also don't use a
mechanical keyboard. I had previously converted a PSION PDA into a PinePhone
keyboard[^3], and it really spoiled me - I still prefer its firm, silent typing
experience over the clicky, mushy PinePhone keyboard  - YMMV!

The keyboard is also not backlit, and while I am usually a touch typist, which
makes this not much of an issue on "normal" keyboards, the non-standard PinePhone Keyboard
takes a bit of time to learn and get used to in the dark. Speaking of LEDs:
an indicator for charge level of the keyboards battery or at least a charging
state LED (does it currently power the phone?) would be really helpful. 
 
Then there was another issue that kept me from using the keyboard: I don't like
16:9 laptops, and PinePhones are even wider at 18:9. [Phosh](https://phosh.mobi/), usually my favorite
mobile shell, with its top and bottom bar takes too much of the precious
vertical space. Sure, you can somewhat mitigate this by choosing a different
scaling factor (1.5, 1.25 or even 1.0), but still - swiping up from the bottom
is not much fun with the keyboard attached.

IMHO, Sxmo (with Sway) is a better choice when having a phone - but more on
that later.

### The PinePhone Pro

Let's talk PinePhone Pro. The PinePhone Pro, [announced in October 2021](https://www.pine64.org/2021/10/15/october-update-introducing-the-pinephone-pro/) was
supposed to solve the performance issues of the original, AllWinner
A64-"powered" PinePhone and deliver a more mainstream compatible, mainline
Linux phone.

For (potentially) various reasons, it has not lived up to this promise (yet). Almost two
years in, battery life remains abysmal - standby is okay, but don't you
browse the web or do something else. I have not conducted measurements, but
I think less than two hours of active use battery life is a fair estimate -
depending on what you do it may be less - even only 45 minutes.

That said, it's not like there are no laudable developments. To name on
example the cameras work with Snapshot and other apps that support the
libcamera+pipewire stack.[^4] Granted, the main camera stops working after
the first wake-up from suspend, but still - it's an important achievement,
as this also means camera support in Firefox these days and thus video chat.

### The surprise

To summarize: I had two things lying around, that both had their deficits.
Combined, however, it's a lot better - as a micro laptop, the PinePhone Pro
is surprisingly fun and useful.

Now how do I use it?

Let's start with the use-case. I don't use the device as a phone, but as a
LTE-enabled micro-laptop. I decided to use [Sxmo](https://sxmo,org) (on [postmarketOS](https://postmarketos.org/), at first edge,
now v23.12), as I like having a large
number of virtual workspaces, and - due to sticking with the default 200%
scaling, mostly terminal apps.

Why? Many GUI apps simly don't work well with a screen height of 360 points
minus swaybar. In theory, fully adaptive apps (at least for GNOME) [should work](https://developer.gnome.org/hig/guidelines/adaptive.html),
but ... I totally get why many don't work on a height of 294 pixels/points.

Also, Sxmo is very convienient when connecting a display and keyboard to the device.
It's like your usual tiling desktop PC, only, due to hardware limitations, a tad slower.
Virtual workspaces are just nice.
 
I could go through all the apps I use, but who has the time. Beyond some nice
mobile apps, like [Warp](https://linuxphoneapps.org/apps/app.drey.warp/), [Tuba](https://linuxphoneapps.org/apps/dev.geopjr.tuba/) and [Authenticator](https://linuxphoneapps.org/apps/com.belmoussaoui.authenticator/), I use a bunch of terminal apps, including, but not limited to, [tut](https://tut.anv.nu/), [aerc](https://aerc-mail.org/), [vim](https://www.vim.org/), and [emacs](https://www.gnu.org/software/emacs/) (for [Org Mode](https://orgmode.org/)).
In fact, while I am still quite inexperienced and not super effective in emacs and Org Mode,
I consider it the killer app of this micro laptop.

I browse the Web with Firefox, made useful by [user0](https://codeberg.org/user0/mobile-config-firefox/src/branch/fenix) and enhanced by [uMatrix](https://addons.mozilla.org/en-US/firefox/addon/umatrix/) and [Tridactyl](https://tridactyl.xyz/about/).

Do I need to mention that I don't use this for phone calls?

### Related Video

If you prefer visual things with audio, I recorded a video for you:

<iframe title="How I started using the PinePhone Pro after giving up on it + FOSDEM 2024 preview" width="560" height="315" src="https://tilvids.com/videos/embed/712c594c-2e68-4aee-9172-dd73c4131f6e" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

Video link: [TilVids](https://tilvids.com/w/eYyhq5SJcY9DzF3VSpUE5w)

### Addition, 2024-03-14

I seem to have [hardware issues with the Keyboard again](https://fosstodon.org/@linmob/112095728804344846) - likely solvable. (I write "seem to", because it might just be bad luck with seating the connector - I've had that before.)


[^1]: After the EeePC, a number of ARM-powered Chromebooks, mostly booting Arch
Linux ARM served that niche.

[^2]: I keep watching buy-stuff-used websites for an affordable variant of
the SF2 Go with 256 GB storage and LTE, and am really disappointed with the
Surface Go 4, that has dropped LTE and still is knee-capped with only 8GB
of RAM and non-user-upgradable storage, which really killed my initial
excitement about seeing the N200 Alder Lake APU instead of Amber Lake Y.

[^3]: See the first seconds of the mid-2021 [PinePhone software progress video](https://www.youtube.com/watch?v=C4RC3Miuo2A) for a visual. I bought a broken Psion MX 5, and
[this adapter on tindie], and cobbled it together. The keyboard is then being
connected to the phone via USB-C.

[^4]: To make flatpak apps work with the camera, you need a proper portals setup. Installing `xdg-desktop-portal-gtk`, `xdg-desktop-portal-wlr` and `sway-portalsconf` did the trick for me. For Phosh users: Installing `phosh-portalsconf`
instead of the `sxmo-...` package should do the trick for you.
