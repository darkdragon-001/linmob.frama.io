+++
title = "Weekly GNU-like Mobile Linux Update (30/2023): Ubuntu Touch 20.04 OTA 2 and NemoMobile Qt 6 progress "
date = "2023-07-30T20:44:30Z"
draft = false 
[taxonomies]
tags = ["Ubuntu Touch", "Nemo Mobile", "Droidian", "lowendlibre", "ClockworkPi", "Megapixels",]
categories = ["weekly update"]
authors = ["Peter",]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Also: Megapixels sees a release and work on Librem 5 support, some Droidian showcases, and more!
<!-- more -->
_Commentary in italics._

### Hardware 
- [ClockworkPi uConsole is now shipping (modular portable computer & game console) - Liliputing](https://liliputing.com/clockworkpi-uconsole-is-now-shipping-modular-portable-computer-game-console/)

### Software

#### Gnome Ecosystem
- This Week in GNOME: [#106 GUADEC 2023](https://thisweek.gnome.org/posts/2023/07/twig-106/)
- GNOME Shell & Mutter: [GNOME Shell styling changes: A PSA for theme authors](https://blogs.gnome.org/shell-dev/2023/07/26/gnome-shell-styling-changes-a-psa-for-theme-authors/)

#### Plasma Ecosystem
- Nate Graham: [This week in KDE: Sounds like Plasma 6](https://pointieststick.com/2023/07/28/this-week-in-kde-sounds-like-plasma-6/)
- Nate Graham: [What we plan to remove in Plasma 6](https://pointieststick.com/2023/07/26/what-we-plan-to-remove-in-plasma-6/)
- dot.kde.org: [Akademy 2023 - How it Happened](https://dot.kde.org/2023/07/25/akademy-2023-how-it-happened)

#### Ubuntu Touch
- UBports News: [Ubuntu Touch OTA-2 Focal Release](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-2-focal-release-3894)
  - Ubuntu Touch Forums News: [Ubuntu Touch OTA-2 Focal](https://forums.ubports.com/topic/9183/ubuntu-touch-ota-2-focal)
  - Liliputing: [Ubuntu Touch 20.04 OTA-2 brings support for more smartphones](https://liliputing.com/ubuntu-touch-20-04-ota-2-brings-support-for-more-smartphones/)
- UBports News: [Ubuntu Touch OTA-2 for FOCAL Call for Testing](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-ota-2-for-focal-call-for-testing-3892)
  - Ubuntu Touch Forums News: [20.04 OTA-2 Call for Testing](https://forums.ubports.com/topic/9155/20-04-ota-2-call-for-testing)
- Unofficial Ubuntu Touch for PinePhone (Pro): [v0.8.1](https://gitlab.com/ook37/pinephone-pro-debos/-/tags/v0.8.1). _Make sure to read the release notes!_
- [Codemaster Freders (@fredldotme): "After hacking on a \*bunch\* of things I finally got Wireless Display working on the JingPad, and I even made the Lomiri desktop multi-monitor capable specifically for tablets. Did someone just say "iPad killer for work tasks"?"](https://nitter.net/fredldotme/status/1683410691858767874#m)

#### Nemo Mobile
- Nemo Mobile UX team: [Nemomobile in July 2023](https://nemomobile.net/pages/nemomobile-in-july-2023/). _Great round-up!_
- [neochapay (@neochapay): "Hello first start #nemomobile on #Qt6 We have done a great job of porting MER middleware to Qt6. On next week we start create weekly builds images of nemo with Qt6 for #pine64 #pinephone"](https://nitter.net/neochapay/status/1683822190607716352#m)

#### Distributions
- [Droidian Linux: "Droidian Linux with Phosh running on Pixel 3A showcasing working DT2W (Double Tap to Wake) and Fingerprint sensor!"](https://fosstodon.org/@droidian/110777384527876669). _Impressive!_
- [Droidian Linux: "Samsung S9 just received full disk encryption…"](https://fosstodon.org/@droidian/110800541863226277)
- [Droidian Linux: "Mainline phones might not have proper battery management but #Droidian does!…"](https://fosstodon.org/@droidian/110794427887532485). _Is it really necessary to say bad things about each other in such a tiny niche?_

#### Matrix
- Matrix.org: [This Week in Matrix 2023-07-28](https://matrix.org/blog/2023/07/21/this-week-in-matrix-2023-07-28/)
- Matrix.org: [Postponing the Libera.Chat deportalling](https://matrix.org/blog/2023/07/postponing-libera-chat-deportalling/)

#### Kernel
- phone-devel: [[PATCH v2 0/9] Initial Marvell PXA1908 support](http://lore.kernel.org/phone-devel/20230727162909.6031-1-duje.mihanovic@skole.hr/)

### Worth Noting
- [Martijn Braam: "GPU accelerated preview on the #librem5 with #megapixels :D…"](https://fosstodon.org/@martijnbraam/110775163438234897). _This week also saw the [release of megapixels 1.7.0](https://gitlab.com/postmarketOS/megapixels/-/commits/1.7.0?ref_type=tags), but that contains other cool features, not this yet._
- [𖤐 MadameMalady 𖤐: "A new update to My Waydroid Manager…"](https://strangeobject.space/@FOSSingularity/110787847705592120)
- [justsoup: "As promised, here is a photo of the TCL A3 (bangkok_tf) booting postmarketOS. 🎉 …"](https://mstdn.social/@justsoup/110776480566092758)
- [Wayland.social: "Welcome to Wayland.social: the successor to X.com…"](https://wayland.social/@compositor/110768798303454842)
- Lemmy - linuxphones: [Anyone using Lemmy on a GNU/Linux phone?](https://lemmy.ml/post/2371839)
- r/MobileLinux: [Donation with best bang for the buck](https://www.reddit.com/r/mobilelinux/comments/157wt6w/donation_with_best_bang_for_the_buck/)
- PinePhone (Reddit): [Install 9999 packages on postmarketOS](https://www.reddit.com/r/pinephone/comments/15832gk/install_9999_packages_on_postmarketos/)
- PinePhone (Reddit): [Modem firmware 0.7.4 and Gnome Firmware?](https://www.reddit.com/r/pinephone/comments/157o7ez/modem_firmware_074_and_gnome_firmware/)
- PINE64official (Reddit): [Pinetab-V arrived today.](https://www.reddit.com/r/PINE64official/comments/15cnoz6/pinetabv_arrived_today/)
- Lemmy - postmarketOS: [Support for RISC-V PineTab 2?](https://lemmy.ml/post/2491279)
- [Sebastian Krzyszkowiak: "This month 15 years have passed since I've received my Openmoko Neo FreeRunner, which started my personal adventure with mobile GNU/Linux 😊 …"](https://social.librem.one/@dos/110792983567774422)

### Worth Reading
- inki: [so, i wanted to write that i guess today we are l… -](https://xn--y9azesw6bu.xn--y9a3aq/content/c805aeda-ee03-4195-a78b-0ae106b1176b/). _About the Droid 4 running Maemo Leste._
- pesader: [1st Contribution Hackathon, by LKCAMP](https://pesader.dev/posts/1st-contribution-hackathon/)
- hughsie: [Introducing Passim](https://blogs.gnome.org/hughsie/2023/07/28/introducing-passim/)
- Tobias Bernard: [Rethinking Window Management](https://blogs.gnome.org/tbernard/2023/07/26/rethinking-window-management/)
  - Phoronix: [GNOME Developers Working To Rethink Their Window Management Approach](https://www.phoronix.com/news/GNOME-Re-Do-Tiling-WM)
- Laoblog: [Pinephone short review: Manjaro beta 34](https://blog.libero.it/Laoblog2/16673458.html)
- Carl Schwan: [Mastodon and Matrix link in your AppStream file](https://carlschwan.eu/2023/07/25/mastodon-and-matrix-link-in-your-appstream-file/)
- Drew DeVault: [Alpine Linux does not make the news](https://drewdevault.com/2023/07/25/Alpine-does-not-make-news.html)
- Neil Brown: [Snikket, a self-contained XMPP distribution](https://neilzone.co.uk/2023/07/snikket-a-self-contained-xmpp-distribution)
- Purism: [Purism’s Guide to Security- Internet & Tech Decentralization](https://puri.sm/posts/purisms-guide-to-security-internet-tech-decentralization/)


### Worth Watching
- RTP Tech Tips: [Pinetab2 Ubuntu Touch Can Install Linux Apps](https://www.youtube.com/watch?v=Uk9R1spA1tg)
- Continuum Gaming: [Continuum Gaming E375: Sailfish OS app – Situations](https://www.youtube.com/watch?v=da6_qerScfE)
- Lup Yuen Lee: [What’s inside a Smartphone? Exploring the internals with Apache NuttX Real-Time Operating System](https://www.youtube.com/watch?v=Wnrq9BlNP9o)
- Louis Rossmann: [Purism ghosts Librem 5 customer, lies about refund policy - AVOID!](https://www.youtube.com/watch?v=wKegmu0V75s) _I really hope Purism stop this behaviour._
  - Purism (Reddit): [Well, Purism seems to have some explaining to do](https://www.reddit.com/r/Purism/comments/15c5k0w/well_purism_seems_to_have_some_explaining_to_do/)
- Baonks 81: [Dualboot Android and postmarketOS community pre-built image on Micro SDcard](https://www.youtube.com/watch?v=Iaieg-NRFx8)
- Jhony Kernel: [Install maemo leste chimaera di Nokia N900](https://www.youtube.com/watch?v=QvTvHe8R1l0)

### Thanks
Huge thanks to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speed up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

